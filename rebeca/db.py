"""Module for debugging neural network models."""
import pprint
from pathlib import Path

import neptune.new as neptune
import numpy as np
import pandas as pd
import torch
import torch.nn as nn
import torch.optim as optim
import torch_geometric as tg
from torch.utils.data import DataLoader
from torch.utils.data.sampler import SequentialSampler
from tqdm import tqdm

import rebeca.data.dataset as da
import rebeca.model.sequence_model as sm
from rebeca.core import load_cfg, parse_args
from rebeca.data.dataset import GroupedBatchSampler, UTRDataset

# from rebeca.model.sequence_model import DoubleConv, init_weights,CONVBILSTM,REC,MLP
from rebeca.util.training_utils import (
    evaluate,
    save_checkpoint,
    train_batch,
    train_graph,
    val_batch,
)


def main():  # noqa: CCR001
    """Main script."""
    # Selecting hardware
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    print(f"Using {device} device")

    # Reproducibility settings
    seed = 3
    np.random.seed(seed)
    torch.manual_seed(seed)
    torch.backends.cudnn.deterministic = True
    torch.backends.cudnn.benchmark = False  # switch on optimiser for cuDNN algorithms for computing convolutions, switch off for reproducibility

    # Load args
    args = parse_args()

    # Load config.yml
    cfg = load_cfg(args.config / "config.yml")

    # Print selected configuration
    pp = pprint.PrettyPrinter(indent=4)
    pp.pprint(cfg)

    # Load model
    model = getattr(sm, cfg["architecture"]["name"])(**cfg["param_architecture"])
    model.apply(sm.init_weights)
    print("the model is", model)
    nb_param = sum(
        p.numel() for p in model.parameters() if p.requires_grad
    )  # get the number of trainable parameters.
    print(f"the model has {nb_param} parameters.")

    # Specifying hardware
    if torch.cuda.is_available():
        # if torch.cuda.device_count() > 1:
        #     model=tg.nn.DataParallel(model)
        model.to(device)  # you need to send your model weights to the GPU
        num_workers = cfg["train"]["num_workers"] * torch.cuda.device_count()
        batch_size = cfg["train"]["batch_size"] * torch.cuda.device_count()
    else:
        num_workers = 0
        batch_size = cfg["train"]["batch_size"]

    # Load data

    # train_dataloader = tg.loader.DataLoader(da.RNA2D(
    # root='rebeca/data/kuo/graph/fepb/cache/',
    # path_data='rebeca/data/kuo/graph/fepb/ex_vienna/',
    # df_path='rebeca/data/kuo/graph/fepb/ex_vienna/fepb_seq_fluo_distribution.csv',
    # graph_type=''
    # ,split=''), batch_size=3,shuffle=True
    #     )

    train_dataloader = tg.loader.DataLoader(
        getattr(da, cfg["dataset"])(split="", **cfg["param_dataset"]),
        batch_size=cfg["train"]["batch_size"],
        shuffle=True,
    )


    # train_dataloader=tg.loader.DataLoader(
    #         dataset, batch_size=3,shuffle=True
    #     )

    # for batch in train_dataloader:
    #     print(batch)
    # train_dataloader = DataLoader(
    #     getattr(da, cfg['dataset'])(split='train',**cfg['param_dataset']),
    #     batch_size=batch_size,
    #     shuffle=True,
    #     num_workers=num_workers,
    #     pin_memory=True,
    # )
    # val_dataloader = DataLoader(
    #     getattr(da, cfg['dataset'])(split='val',**cfg['param_dataset']),
    #     batch_size=batch_size,
    #     shuffle=True,
    #     num_workers=num_workers,
    #     pin_memory=True,
    # )

    # else:
    #     dataset = UTRDataset(journal=f"{args.data_name}", split="train", utr_max_length=30)
    #     idx_len=list(pd.read_csv('rebeca/data/nar_np_train.csv')['UTR'].str.len())
    #     sampler=SequentialSampler(dataset)
    #     cu_batch_sampler=GroupedBatchSampler(sampler,idx_len,64)
    #     train_dataloader=DataLoader(dataset,batch_sampler=cu_batch_sampler)
    #
    #     dataset = UTRDataset(journal=f"{args.data_name}", split="val", utr_max_length=30)
    #     idx_len=list(pd.read_csv('rebeca/data/nar_np_val.csv')['UTR'].str.len())
    #     sampler=SequentialSampler(dataset)
    #     cu_batch_sampler=GroupedBatchSampler(sampler,idx_len,64)
    #     val_dataloader=DataLoader(dataset,batch_sampler=cu_batch_sampler)

    # Declare loss function and optimizer
    loss_fn = getattr(nn, cfg["loss"]["name"])()
    optimizer = getattr(optim, cfg["optimizer"]["name"])(
        model.parameters(), lr=float(cfg["optimizer"]["lr"])
    )

    # layer = BinningLayer(4,10)
    # normal_tensor=torch.tensor([[0.0,1.0],[15.0,1.0],[3.0,2.0]])
    # print(layer(normal_tensor))

    best_loss = 100
    best_epoch = 0
    num_epochs, warmup_epochs, patience = (
        int(cfg["train"]["num_epochs"]),
        int(cfg["train"]["warmup_epochs"]),
        int(cfg["train"]["patience"]),
    )
    # data = dataset[0].to(device)
    # print('ok, now it ',data.y)

    # def train_step(model: nn.Module, data, loss_fn, optimizer, device):
    #     """Training step using a batch of data."""
    #     data = data.to(device)
    #     # print('aebrfiuyaberol',model(data))
    #     logits,_= model(data)  # (batch, num_classes)
    #     loss_train = loss_fn(logits, data.y)
    #     loss_train.backward()
    #     optimizer.step()
    #     optimizer.zero_grad()

    #     # preds = logits.argmax(dim=1)  # (batch,)
    #     # num_correct_train = int((preds == data.y).sum().cpu().numpy())
    #     # num_sample_train = len(preds)
    #     # acc_train = num_correct_train / num_sample_train
    #     return float(loss_train.detach().cpu().numpy())

    for _ in range(100):  # loop over the dataset multiple times
        loss = []
        acc = []
        # loss_val = []
        # loop = tqdm(train_dataloader, total=int(len(train_dataloader)))
        model.train()
        for data_train in list(train_dataloader):  # [:1]:

            loss = train_graph(model, data_train, loss_fn, optimizer, device)
            # print('ok, now it ',data_train.y)
            print(loss)

            # inputs, labels, _ = data  # get the inputs; data is a list of [inputs, labels]
            # inputs, labels = inputs.to(device), labels.to(device)  # Send input tensors to the GPU
            # loss_batch, acc_batch = train_batch(model, inputs, labels, loss_fn, optimizer)
            # loss.append(loss_batch)
            # acc.append(acc_batch)


if __name__ == "__main__":
    main()
