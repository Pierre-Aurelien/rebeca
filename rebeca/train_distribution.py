"""Module for training neural network architectures."""
import os
import pprint
from pathlib import Path

import neptune.new as neptune
import numpy as np
import pandas as pd
import torch
import torch.nn as nn
import torch.optim as optim
from torch.utils.data import DataLoader
from torch.utils.data.sampler import SequentialSampler
from tqdm import tqdm

import rebeca.data.dataset as da
import rebeca.model.sequence_model as sm
from rebeca.core import load_cfg, parse_args
from rebeca.data.dataset import GroupedBatchSampler, UTRDataset

# from rebeca.model.sequence_model import DoubleConv, init_weights,CONVBILSTM,REC,MLP
from rebeca.util.training_utils import evaluate, save_checkpoint, train_batch, val_batch


def main():  # noqa: CCR001
    """Main script."""
    # Selecting hardware
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    print(f"Using {device} device")

    # Reproducibility settings
    seed = 3
    np.random.seed(seed)
    torch.manual_seed(seed)
    torch.backends.cudnn.deterministic = True
    torch.backends.cudnn.benchmark = False  # switch on optimiser for cuDNN algorithms for computing convolutions, switch off for reproducibility

    # Load args
    args = parse_args()

    # Experiment Tracking
    if args.neptune:
        run = neptune.init(
            project="pag/rebeca",
            api_token=os.environ["NEPTUNE_API_TOKEN"],
            capture_stdout=False,
            capture_stderr=False,
        )
        with open(Path(args.config) / "neptune_run.txt", mode="w") as f:
            f.write(f"The url for the neptune run is {run.get_run_url()}")

    # Load config.yml
    cfg = load_cfg(args.config / "config.yml")

    # Print selected configuration
    pp = pprint.PrettyPrinter(indent=4)
    pp.pprint(cfg)

    # Load architecture
    architecture = getattr(sm, cfg["architecture"]["name"])(**cfg["param_architecture"])
    architecture.apply(sm.init_weights)
    print("the architecture is", architecture)
    nb_param = sum(
        p.numel() for p in architecture.parameters() if p.requires_grad
    )  # get the number of trainable parameters.
    print(f"the architecture has {nb_param} parameters.")

    # Specifying hardware
    if torch.cuda.is_available():
        if torch.cuda.device_count() > 1:
            architecture = nn.DataParallel(architecture)
        architecture.to(device)  # you need to send your model weights to the GPU
        num_workers = cfg["train"]["num_workers"] * torch.cuda.device_count()
        batch_size = cfg["train"]["batch_size"] * torch.cuda.device_count()
    else:
        num_workers = 0
        batch_size = cfg["train"]["batch_size"]

    # Load data
    train_dataloader = DataLoader(
        getattr(da, cfg["dataset"])(split="train", **cfg["param_dataset"]),
        batch_size=batch_size,
        shuffle=True,
        num_workers=num_workers,
        pin_memory=True,
    )
    val_dataloader = DataLoader(
        getattr(da, cfg["dataset"])(split="val", **cfg["param_dataset"]),
        batch_size=batch_size,
        shuffle=True,
        num_workers=num_workers,
        pin_memory=True,
    )

    # else:
    #     dataset = UTRDataset(journal=f"{args.data_name}", split="train", utr_max_length=30)
    #     idx_len=list(pd.read_csv('rebeca/data/nar_np_train.csv')['UTR'].str.len())
    #     sampler=SequentialSampler(dataset)
    #     cu_batch_sampler=GroupedBatchSampler(sampler,idx_len,64)
    #     train_dataloader=DataLoader(dataset,batch_sampler=cu_batch_sampler)
    #
    #     dataset = UTRDataset(journal=f"{args.data_name}", split="val", utr_max_length=30)
    #     idx_len=list(pd.read_csv('rebeca/data/nar_np_val.csv')['UTR'].str.len())
    #     sampler=SequentialSampler(dataset)
    #     cu_batch_sampler=GroupedBatchSampler(sampler,idx_len,64)
    #     val_dataloader=DataLoader(dataset,batch_sampler=cu_batch_sampler)

    # Declare loss function and optimizer
    loss_fn = getattr(nn, cfg["loss"]["name"])()
    optimizer = getattr(optim, cfg["optimizer"]["name"])(
        architecture.parameters(), lr=float(cfg["optimizer"]["lr"])
    )

    best_loss = 100
    best_epoch = 0
    num_epochs, warmup_epochs, patience = (
        int(cfg["train"]["num_epochs"]),
        int(cfg["train"]["warmup_epochs"]),
        int(cfg["train"]["patience"]),
    )
    for epoch in range(num_epochs):  # loop over the dataset multiple times
        architecture.train()
        loss = []
        loss_val = []
        loop = tqdm(train_dataloader, total=int(len(train_dataloader)))
        for _, data in enumerate(loop):
            inputs, labels, _ = data  # get the inputs; data is a list of [inputs, labels]
            inputs, labels = inputs.to(device), labels.to(device)  # Send input tensors to the GPU
            loss_batch = train_batch(architecture, inputs, labels, loss_fn, optimizer)
            loss.append(loss_batch)
            if args.neptune:
                # log on neptune
                run["train/batch/loss"].log(loss_batch)
        if args.neptune:
            # log on neptune
            run["train/batch/epoch_loss"].log(np.mean(loss))
        # update progress bar
        loop.set_postfix(loss=np.mean(loss))
        loop.set_description(f"Epoch [{epoch}/{num_epochs}]")

        with torch.no_grad():
            architecture.eval()
            for data in val_dataloader:
                inputs, labels, _ = data
                inputs, labels = inputs.to(device), labels.to(
                    device
                )  # Send input tensors to the GPU
                _, loss_val_batch, acc_val_batch, _ = val_batch(
                    architecture, inputs, labels, loss_fn
                )
                loss_val.append(loss_val_batch)
                acc_val.append(acc_val_batch)
                if args.neptune:
                    run["val/batch/accuracy"].log(acc_val_batch)
                    run["val/batch/loss"].log(loss_val_batch)
            val_loss_epoch = np.mean(loss_val)
            if args.neptune:
                run["train/distribution"].log(
                    evaluate(val_dataloader, architecture, epoch, "validation", device)
                )
                run["val/batch/epoch_accuracy"].log(np.mean(acc_val_batch))
                run["val/batch/epoch_loss"].log(np.mean(loss_val))
            if val_loss_epoch < best_loss and epoch > warmup_epochs:
                best_loss = val_loss_epoch
                best_epoch = epoch
                # keep snapshot
                best_model = architecture.state_dict()
        if epoch - best_epoch > patience:
            print("training finished")
            if args.neptune:
                # Log PyTorch model weights
                run["model_checkpoints/my_model"].upload(
                    f"model_checkpoints/best_model_epoch_{epoch}.pth"
                )
            break
    # At this point save a snapshot of the best model
    torch.save(best_model, Path(args.config) / f"best_model_epoch_{best_epoch}.pth")
    if args.neptune:
        run.stop()


if __name__ == "__main__":
    main()
