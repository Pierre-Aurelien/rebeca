"""Modules with loss functions."""
import torch
from torch import nn
from torch.nn import functional as F


def kl_divergence(output,target):
    """
    Computes the KL divergence between two normals.
    KL(N(m1,s1), N(m2,s2)) = log(s2/s1) + (s1^2 + (m1-m2)^2)/(2*s2^2) - 1/2
    m1= first axis in predictions (mean)
    log(s1)= second axis in predictions (mean)
    m2= Ground truth mean
    s2= Ground truth standard deviation
    """
    m1=output[:,0]
    log_s1=output[:,1]
    s1=torch.exp(output[:,1])
    m2=target[:,0]
    s2=target[:,1]
    num=torch.square(s1)+torch.square(m1-m2)
    denom=torch.square(s2)+1e-7 #6e-8 is the minimum for float16 (mixed precision training)
    kld_loss = torch.mean(0.5 * (torch.log(s2)-log_s1+torch.div(num,denom)-1), dim = 0)
    return kld_loss
