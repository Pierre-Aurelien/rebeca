"""EXP_25
GOAL: Investigate the baseline of double conv architectures to predict the mean and standard deviation for each construct.
METHOD: simple fepb dataset from kuo et al, using the lightweight CNN architecture. we used the KL divergence loss
"""
