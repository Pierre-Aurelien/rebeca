"""Module for testing the performance of a neural network architectures."""

import pprint
from collections import OrderedDict
from pathlib import Path
from itertools import chain
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
import torch
import torch.nn as nn
from sklearn.metrics import confusion_matrix
from torch.utils.data import DataLoader
from rebeca.util.loss_function import kl_divergence

import rebeca.data.dataset as da
import rebeca.model.sequence_model as sm
from rebeca.core import load_cfg, parse_args
from rebeca.util.testing_utils import bootstrap_estimate_and_ci, draw_metrics, get_output

matplotlib.rcParams["pdf.fonttype"] = 42
matplotlib.rcParams["ps.fonttype"] = 42
plt.rcParams.update({"font.size": 8})


def evaluate(data_loader, model, split, loss_fn,target_type):
    y_t, y_p, y_l, x_s = get_output(data_loader, model, loss_fn,target_type)
    # print('hey',y_t,y_p,y_l,x_s)
    df_aux = pd.DataFrame(
        [[x_s[i], y_p[i], y_t[i]] for i in range(len(x_s))], columns=["seq", "prediction", "truth"]
    )
    df_aux["rbs"] = df_aux.apply(lambda row: row["seq"].replace("Z", ""), axis=1)
    y_pred = (
        df_aux.groupby(["rbs"])[["prediction", "truth"]]
        .agg(lambda x: pd.Series.mode(x)[0])["prediction"]
        .to_list()
    )
    y_true = (
        df_aux.groupby(["rbs"])[["prediction", "truth"]]
        .agg(lambda x: pd.Series.mode(x)[0])["truth"]
        .to_list()
    )

    nb_samples = 1
    if target_type=='gaussian':
        macro_metrics, samples = bootstrap_estimate_and_ci(target_type='gaussian',y_true=y_true, y_pred=y_pred, loss_value=y_l, bs_samples=nb_samples)
        fig_bs = draw_metrics(samples,target_type='gaussian')
        fig='nothing'
    else:
        macro_metrics, samples = bootstrap_estimate_and_ci(y_true, y_pred, y_l, bs_samples=nb_samples)

        # Build confusion matrix
        # fig, ax = plt.subplots(1, 1,figsize=(25, 20)) configuration for slides

        cm = 1 / 2.54  # centimeters in inches
        fig, ax = plt.subplots(1, 1, figsize=(8 * cm, 7 * cm))
        classes = ["0", "1", "2", "3", "4", "5", "6", "7"]
        y_true = [f"{x}" for x in y_true]
        y_pred = [f"{x}" for x in y_pred]
        cf_matrix = confusion_matrix(y_true, y_pred, labels=classes)
        df_cm = pd.DataFrame(cf_matrix, index=[i for i in classes], columns=[i for i in classes])
        # plt.figure(figsize = (12,7))
        # sns.heatmap(df_cm, annot=True, ax=ax, cmap="binary",fmt='d',cbar_ax=ax1, ax=ax0)

        fig, (ax0, ax1) = plt.subplots(ncols=2, gridspec_kw={"width_ratios": [6, 1]})
        sns.heatmap(df_cm, annot=True, cmap="binary", fmt="d", cbar_ax=ax1, ax=ax0)

        for spine in ax0.spines.values():
            spine.set_visible(True)
        for spine in ax1.spines.values():
            spine.set(visible=True, lw=0.8, edgecolor="black")
            plt.ylabel("True label")  # ,fontsize=36)
        plt.xlabel("Predicted label")  # , fontsize=36)

        fig_bs = draw_metrics(samples,target_type='bins')
        # ax.set_title(
        #     f"Confusion matrix evaluated on the {split} set. ACC:{accuracy}% ,ext_ACC={extended_accuracy}%."
        # )
        # ax.text(3, 0.01, f"{metrics}", ha="center", fontsize=18)
        # ax.annotate(
        #     f"{macro_metrics}",
        #     (0, 0),
        #     (0, -60),
        #     xycoords="axes fraction",
        #     textcoords="offset points",
        #     va="top",
        # ) 

    return fig, macro_metrics, samples, fig_bs


def main():  # noqa: CCR001
    """Main script."""
    # Selecting hardware, Load on CPU
    device = torch.device("cpu")
    print(f"Using {device} device")

    # Reproducibility settings
    seed = 3
    np.random.seed(seed)
    torch.manual_seed(seed)
    torch.backends.cudnn.deterministic = True
    torch.backends.cudnn.benchmark = False  # switch on optimiser for cuDNN algorithms for computing convolutions, switch off for reproducibility

    # Load args
    args = parse_args()

    # Load config.yml
    cfg = load_cfg(args.config / "config.yml")

    # Print selected configuration
    pp = pprint.PrettyPrinter(indent=4)
    pp.pprint(cfg)

    # Loadmodel
    model = getattr(sm, cfg["architecture"]["name"])(**cfg["param_architecture"])
    model.apply(sm.init_weights)
    print("the model is", model)

    df_total=pd.read_csv(Path(cfg['param_dataset']['path_data']) / f"all_ft_train.csv")
    list_prom=df_total['promoter'].unique().tolist()
    for promoter in list_prom:
        cfg['param_dataset']['promoter']=promoter # uncomment if want all data in one file
        cfg['model']='ft'
        # Load data
        test_dataloader = DataLoader(
            getattr(da, cfg["dataset"])(split="test", **cfg["param_dataset"]),
            batch_size=30,
            shuffle=False,
            num_workers=cfg["train"]["num_workers"],
            pin_memory=True,
        )

        # Declare loss function and optimizer
        try: 
            loss_fn = getattr(nn, cfg["loss"]["name"])()
        except AttributeError:
            loss_fn=kl_divergence

        if cfg['model']=='bft':
            model_path=Path(args.config) / f"starting_model.pth"
            print('we are using the starting model')
        else:
            model_path=Path(args.config) / f"best_ft_model_promoter_{promoter}.pth"
            print('we are using the fine-tuned model')

        # original saved file with DataParallel
        # epoch = model_path.stem.split("l_")[1]
        state_dict = torch.load(model_path, map_location=device)
        # # create new OrderedDict that does not contain `module.`
        # new_state_dict = OrderedDict()
        # for k, v in state_dict.items():
        #     name = k[7:]  # remove `module.`
        #     new_state_dict[name] = v
        # load params
        # model.load_state_dict(new_state_dict)
        model.load_state_dict(state_dict)
        nb_param = sum(
            p.numel() for p in model.parameters() if p.requires_grad
        )  # get the number of trainable parameters.
        print(f"the model has {nb_param} parameters.")
        fig, macro_metrics, samples, fig_bs = evaluate(test_dataloader, model, "test", loss_fn,target_type='gaussian')
        # print(macro_metrics)
        # print("metrics averaged are", macro_metrics[0, :])
        # plt.tight_layout()
        # # fig.savefig(
        # #     Path(args.config) / f"confusion_matrix_{epoch}.pdf",
        # #     transparent=True,
        # #     bbox_inches="tight",
        # # )
        # sns.set_theme(style="whitegrid")
        # fig_bs.savefig(
        #     Path(args.config) / "performance_nn.pdf", transparent=True, bbox_inches="tight"
        # )
        # with open(Path(args.config) / f"metrics_{epoch}.txt", mode="w") as f:
        #     f.write(
        #         f"The macro metrics (accuracy, extended_accuracy,precision, recall, F-measure) and "
        #         f"their estimates, lower/upper bounds and std_deviation are given by the following 4*5 matrix:\n {macro_metrics}"
        #     )
        # np.save(Path(args.config) / "bootstrap_metrics.npy", samples)

        #Save predictions
        sequence=[]
        output=[]
        g_t=[]
        for data in list(test_dataloader):
            # evaluate on Variable x with testing data
            image, gaussian_parameters, sequence_idx = data
            y = model(image)
            # access Variable's tensor, copy back to CPU, convert to numpy
            output+=y.detach().cpu().tolist()
            g_t+=gaussian_parameters.detach().cpu().tolist()
            sequence+=sequence_idx
            # print('this it gt',g_t,np.array(g_t))
        # write CSV
        # np.savetxt(Path(args.config) /'output.csv', np.array(output))
        # np.savetxt(Path(args.config) /'labels.csv', np.array(g_t))
        # print('hey',Path(args.config),output[:10])
        # model_id=Path(args.config).stem.split("_")[1]
        path_ft=Path(args.config) / 'ft_results'
        path_ft.mkdir(parents=True, exist_ok=True)
        df = pd.DataFrame(data={"seq": sequence,"output": list(chain.from_iterable(output)),"label": list(chain.from_iterable(g_t))})
        df.to_csv(path_ft /f'complete_{promoter}_test.csv', sep=',',index=False)
        
        # Get random baseline
            # Load data
        # train_dataloader = DataLoader(
        #     getattr(da, cfg["dataset"])(split="train", **cfg["param_dataset"]),
        #     batch_size=256,
        #     shuffle=False,
        #     num_workers=cfg["train"]["num_workers"],
        #     pin_memory=True,
        # )
        # # Get mean mu and sd
        # mean_mu=[]
        # mean_sd=[]
        # for data in train_dataloader:
        #     mean_mu+=list(torch.transpose(data)[0,:].cpu())
        #     mean_sd+=list(torch.transpose(data)[1,:].cpu())
        # mean_mu=mean()


if __name__ == "__main__":
    main()
