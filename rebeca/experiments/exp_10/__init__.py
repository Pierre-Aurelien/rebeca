"""EXP_10
GOAL: Investigate the performance  of sequence-based models on different genetic contexts than it was trained on
METHOD: simple fepb dataset from kuo et al, using the heavy Hybrid CNN-RNN architecture. (from EXP_08)
Here we use a lower learning rate 3e-4
the test set was dmsc.csv
"""
