"""Module to define sequence-based models."""
from typing import List

import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F  # noqa: E731
import torch_geometric as tg
from torch_geometric.data import Data
from torch_geometric.nn import BatchNorm, PNAConv, global_add_pool,GCN,GATv2Conv,NNConv,Set2Set,aggr,GCNConv,GraphNorm,TransformerConv
from torch.nn import Embedding, Linear, ModuleList, ReLU, Sequential 

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")


def init_weights(m):
    """Initialise weights."""
    if isinstance(m, nn.Linear):
        # torch.nn.init.kaiming_normal_(m.weight, mode="fan_in", nonlinearity="relu")
        torch.nn.init.xavier_uniform_(m.weight, gain=1.0)


def get_activation(name: str, **kwargs) -> nn.Module:
    """Get an activation layer from name.

    Args:
        name: activation name
        **kwargs:

    Returns:
        activation layer
    """
    if name == "":
        return nn.Identity()
    return getattr(nn, name)(**kwargs)


def build_mlp(
    dim_in: int,
    hidden_size_mlp: int,
    dim_out: int,
    num_layers: int,
    activation: str,
    activation_out: str,
    dropout_rate: float = 0.1,
) -> nn.Sequential:
    """Build an multi layer linear model.

    Args:
        dim_in: input dimension.
        hidden_size_mlp: embedding dimension.
        dim_out: output dimension.
        num_layers: total number of layers
        activation: name of activation for non-final layers.
        activation_out: activation for final layer.
        dropout_rate: probability for dropout layers

    Returns:
        Multi-layer perceptron.
    """
    layers: List[nn.Module] = []
    for i in range(num_layers):
        d_in = dim_in if i == 0 else hidden_size_mlp
        d_out = dim_out if i == num_layers - 1 else hidden_size_mlp
        layers.append(nn.Linear(d_in, d_out))

        act = get_activation(activation_out if i == num_layers - 1 else activation)
        layers.append(act)

        if dropout_rate > 0 and i < num_layers - 1:
            # no dropout for last layer
            layers.append(nn.Dropout(dropout_rate))

    return nn.Sequential(*tuple(layers))


class DoubleConv(nn.Module):
    """First Convolutional architecture."""

    def __init__(
        self,
        num_classes=8,
        channels_cnn=64,
        layers_cnn=2,
        kernel_cnn=4,
        kernel_pool=1,
        hidden_size_mlp=256,
        layers_mlp=3,
        activation: str = "ReLU",
        activation_out: str = "",
        dropout_rate: float = 0,
        last_layer_scaler=20 ,
        length_utr=30,
        in_channels=4,
    ):
        """Initialize class.

        Args:
            classes: number of different classes
        """
        super().__init__()
        self.conv1 = nn.Conv1d(
            in_channels=in_channels, out_channels=channels_cnn, kernel_size=kernel_cnn, padding="same"
        )
        self.pool = nn.MaxPool1d(kernel_size=kernel_pool)
        self.convs = nn.ModuleList(
            [
                nn.Conv1d(
                    in_channels=channels_cnn,
                    out_channels=channels_cnn,
                    kernel_size=kernel_cnn,
                    padding="same",
                )
                for _ in range(layers_cnn)
            ]
        )
        self.conv_acts = nn.ModuleList([get_activation(activation) for _ in range(layers_cnn)])
        n_size = self.forward_conv(torch.rand(size=(1, in_channels, length_utr)))
        self.mlp = build_mlp(
            dim_in=n_size,
            hidden_size_mlp=hidden_size_mlp,
            dim_out=num_classes,
            num_layers=layers_mlp,
            activation=activation,
            activation_out=activation_out,
            dropout_rate=dropout_rate,
        )
        self.llc = last_layer_scaler

    def forward_conv(self, x):
        """Forward pass through the Conv layers to get out dimension."""
        x = x.to(torch.float32)
        x = F.relu(self.conv1(x))
        x = self.pool(x)
        for conv, act in zip(self.convs, self.conv_acts):
            x = conv(x)
            x = act(x)
            x = self.pool(x)
        x = torch.flatten(x, 1)  # flatten all dimensions except batch
        return x.size(1)

    def forward(self, x):
        """Forward pass through the convolutional neural network.

        Args:
            x: input, tensor of size

        Returns:
            Last layer, before softmax activation.
        """
        x = x.to(torch.float32)
        x = F.relu(self.conv1(x))
        x = self.pool(x)
        for conv, act in zip(self.convs, self.conv_acts):
            x = conv(x)
            x = act(x)
            x = self.pool(x)

        x = torch.flatten(x, 1)  # flatten all dimensions except batch
        # classifier
        x = self.mlp(x)
        x = torch.mul(self.llc,x)
        return x


class REC(nn.Module):
    """First recurrent architecture."""

    def __init__(
        self,
        classes=8,
        hidden_size_lstm=10,
        layers_lstm=1,
        bilstm=True,
        hidden_size_mlp=256,
        layers_mlp=3,
        activation: str = "ReLU",
        activation_out: str = "",
        dropout_rate: float = 0,
    ):
        """Initialize class.

        Args:
            classes: number of different classes
        """
        super().__init__()
        self.lstm = nn.LSTM(
            input_size=4,
            hidden_size=hidden_size_lstm,
            num_layers=layers_lstm,
            batch_first=True,
            bidirectional=bilstm,
            dropout=0,
        )

        self.mlp = build_mlp(
            dim_in=(bilstm + 1) * hidden_size_lstm,
            hidden_size_mlp=hidden_size_mlp,
            dim_out=classes,
            num_layers=layers_mlp,
            activation=activation,
            activation_out=activation_out,
            dropout_rate=dropout_rate,
        )

    def forward(self, x):
        """Forward pass through the convolutional neural network.

        Args:
            x: input, tensor of size

        Returns:
            Output of the last layer
        """
        x = x.to(torch.float32)
        x = x.permute(0, 2, 1)  # LSTM layers requires [batch_size, seq_len, features]
        x, (h_n, c_n) = self.lstm(x)  # normally x,xh
        x = x[:, -1, :]
        # h_n = h_n.permute(1, 0,
        #                   2)  # Output was [num_layers * num_directions, batch, hidden_size]. Now [batch,num_layers * num_directions,hidden_size]
        # x= torch.flatten(h_n,
        #                     1)  # flatten all dimensions except batch [batch,num_layers * num_directions*hidden_size]
        x = self.mlp(x)
        return x


class CONVBILSTM(nn.Module):
    """Hybrid CNN/LSTM architecture."""

    def __init__(
        self,
        num_classes=8,
        channels_cnn=64,
        layers_cnn=2,
        kernel_cnn=4,
        kernel_pool=2,
        hidden_size_lstm=10,
        layers_lstm=1,
        bilstm=True,
        hidden_size_mlp=256,
        layers_mlp=3,
        activation: str = "ReLU",
        activation_out: str = "",
        dropout_rate: float = 0,
        last_layer_scaler=20 ,
        in_channels=4,
        length_utr=30,
    ):
        """Initialize class.

        Args:
            classes: number of different classes
        """
        super().__init__()
        self.conv1 = nn.Conv1d(
            in_channels=in_channels, out_channels=channels_cnn, kernel_size=kernel_cnn, padding="same"
        )
        self.pool = nn.MaxPool1d(kernel_size=kernel_pool)
        self.convs = nn.ModuleList(
            [
                nn.Conv1d(
                    in_channels=channels_cnn,
                    out_channels=channels_cnn,
                    kernel_size=kernel_cnn,
                    padding="same",
                )
                for _ in range(layers_cnn)
            ]
        )
        self.conv_acts = nn.ModuleList([get_activation(activation) for _ in range(layers_cnn)])
        n_features_lstm, n_length = self.forward_conv(torch.rand(size=(1, in_channels, length_utr)))  # noqa: W0612
        self.lstm = nn.LSTM(
            input_size=n_features_lstm,
            hidden_size=hidden_size_lstm,
            num_layers=layers_lstm,
            batch_first=True,
            bidirectional=bilstm,
            dropout=0,
        )

        self.mlp = build_mlp(
            dim_in=(bilstm + 1) * hidden_size_lstm,
            hidden_size_mlp=hidden_size_mlp,
            dim_out=num_classes,
            num_layers=layers_mlp,
            activation=activation,
            activation_out=activation_out,
            dropout_rate=dropout_rate,
        )
        self.llc = last_layer_scaler

    def forward_conv(self, x):
        """Forward pass through the Conv layers to get out dimension."""
        x = x.to(torch.float32)
        x = F.relu(self.conv1(x))
        x = self.pool(x)
        for conv, act in zip(self.convs, self.conv_acts):
            x = conv(x)
            x = act(x)
            x = self.pool(x)
        return x.size(1), x.size(1)

    def forward(self, x):
        """Forward pass through the convolutional neural network.

        Args:
            x: input, tensor of size

        Returns:
            Output of the last layer
        """
        x = x.to(torch.float32)
        x = F.relu(self.conv1(x))
        x = self.pool(x)
        for conv, act in zip(self.convs, self.conv_acts):
            x = conv(x)
            x = act(x)
            x = self.pool(x)
        x = x.permute(0, 2, 1)  # LSTM layers requires [batch_size, seq_len, features]
        x, (h_n, c_n) = self.lstm(x)  # normally x,xh # noqa: W0612
        h_n = h_n.permute(
            1, 0, 2
        )  # Output was [num_layers * num_directions, batch, hidden_size]. Now [batch,num_layers * num_directions,hidden_size]
        h_n = torch.flatten(
            h_n, 1
        )  # flatten all dimensions except batch [batch,num_layers * num_directions*hidden_size]
        h_n = self.mlp(h_n) #  concatenation of the final forward and reverse hidden states
        h_n = torch.mul(self.llc,h_n)
        return h_n


class MLP(nn.Module):
    """First Convolutional architecture."""

    def __init__(
        self,
        classes=8,
        hidden_size_mlp=256,
        layers_mlp=3,
        activation: str = "ReLU",
        activation_out: str = "",
        dropout_rate: float = 0,
    ):
        """Initialize class.

        Args:
            classes: number of different classes
        """
        super().__init__()
        self.mlp = build_mlp(
            dim_in=30 * 4,
            hidden_size_mlp=hidden_size_mlp,
            dim_out=classes,
            num_layers=layers_mlp,
            activation=activation,
            activation_out=activation_out,
            dropout_rate=dropout_rate,
        )

    def forward(self, x):
        """Forward pass through the convolutional neural network.

        Args:
            x: input, tensor of size

        Returns:
            Last layer, before softmax activation.
        """
        x = x.to(torch.float32)
        x = torch.flatten(x, 1)
        # classifier
        x = self.mlp(x)
        return x


class ConvBin(nn.Module):
    """First Convolutional architecture."""

    def __init__(
        self,
        classes=2,
        channels_cnn=64,
        layers_cnn=2,
        kernel_cnn=4,
        kernel_pool=1,
        hidden_size_mlp=256,
        layers_mlp=3,
        activation: str = "ReLU",
        activation_out: str = "",
        dropout_rate: float = 0,
        bins: int = 8,
        f_max: float = 1e4,
    ):
        """Initialize class.

        Args:
            classes: number of different classes
        """
        super().__init__()
        self.conv1 = nn.Conv1d(
            in_channels=4, out_channels=channels_cnn, kernel_size=kernel_cnn, padding="same"
        )
        self.pool = nn.MaxPool1d(kernel_size=kernel_pool)
        self.convs = nn.ModuleList(
            [
                nn.Conv1d(
                    in_channels=channels_cnn,
                    out_channels=channels_cnn,
                    kernel_size=kernel_cnn,
                    padding="same",
                )
                for _ in range(layers_cnn)
            ]
        )
        self.conv_acts = nn.ModuleList([get_activation(activation) for _ in range(layers_cnn)])
        n_size = self.forward_conv(torch.rand(size=(1, 4, 30)))
        self.mlp = build_mlp(
            dim_in=n_size,
            hidden_size_mlp=hidden_size_mlp,
            dim_out=classes,
            num_layers=layers_mlp,
            activation=activation,
            activation_out=activation_out,
            dropout_rate=dropout_rate,
        )
        self.bins = bins
        self.f_max = f_max

    def forward_conv(self, x):
        """Forward pass through the Conv layers to get out dimension."""
        x = x.to(torch.float32)
        x = F.relu(self.conv1(x))
        x = self.pool(x)
        for conv, act in zip(self.convs, self.conv_acts):
            x = conv(x)
            x = act(x)
            x = self.pool(x)
        x = torch.flatten(x, 1)  # flatten all dimensions except batch
        return x.size(1)

    def forward(self, x):
        """Forward pass through the convolutional neural network.

        Args:
            x: input, tensor of size

        Returns:
            Last layer, before softmax activation.
        """
        x = x.to(torch.float32)
        x = F.relu(self.conv1(x))
        x = self.pool(x)
        for conv, act in zip(self.convs, self.conv_acts):
            x = conv(x)
            x = act(x)
            x = self.pool(x)

        x = torch.flatten(x, 1)  # flatten all dimensions except batch
        # classifier
        x = self.mlp(x)
        x = torch.exp(x)
        x = BinningLayer(self.bins, self.f_max)(x)
        return x


class BaseGCN(torch.nn.Module):
    def __init__(
        self,
        in_node_channels,
        in_edge_channels,
        dim_gnn_embedding,
        num_gnn_layers,
        act_gnn,
        gnn_norm,
        dropout_gnn,
        readout_layer,
        processing_steps_readout,
        hidden_size_mlp,
        num_layers_mlp,
        dropout_mlp,
        act_mlp,
        activation_out,
        num_classes,
    ):
        """Initialise Base GCN class

        Args:
            in_node_channels: dimension of node input feature
            in_edge_channels: dimension of edge input feature
            width_node_embedding: width of MLP for node/edges features embedding
            dim_features_embedding: dimension of node features/edges embedding
            num_features_node_embedding: number fo MLP layers for node/features embedding
            activation_embedding: activation function for node/edges embedding
            dropout_embedding: dropout rate for node/edge embedding
            dim_gnn_embedding: hidden dimension of GNN.
            num_layers_gnn: numebr of gnn layers
            act_gnn: activation function for gnn
            dropout_gnn: dropout used in gnn
            gnn_norm: applies graph normalization over individual graphs
            dim_gnn_embedding: size of the node gnn embeddings
            processing_steps_readout:number of set2set lstm iterations
            readout_layer: type of readoutlayer
            num_layers_mlp: number of Linear layers for classification .
            hidden_size_mlp: width of classification MLP.
            num_classes: number of classes in output
            dropout_mlp: dropout rate of classification MLP.
            act_mlp: name of the activation for the mlp
        """    
        super().__init__()

        self.convs=ModuleList()
        self.gnn_norm=gnn_norm
        self.conv_norm = ModuleList()
        self.conv_act = ModuleList()
        for i in range(num_gnn_layers):
            dim_in = in_node_channels if i == 0 else dim_gnn_embedding
            conv = GCNConv(in_channels=dim_in, out_channels=dim_gnn_embedding)
            self.convs.append(conv)
            self.conv_norm.append(GraphNorm(in_channels=dim_gnn_embedding))
            self.conv_act.append(get_activation(act_gnn))

        self.readout_layer=readout_layer

        if self.readout_layer=='set2set':
            dim_in_mlp=dim_gnn_embedding*2
            self.s2s=Set2Set(in_channels=dim_gnn_embedding,processing_steps=processing_steps_readout)
        if self.readout_layer=='all':
            dim_in_mlp=dim_gnn_embedding*4
        if self.readout_layer=='max':
            dim_in_mlp=dim_gnn_embedding

        self.mlp = build_mlp(
            dim_in=dim_in_mlp,
            hidden_size_mlp=hidden_size_mlp,
            dim_out=num_classes,
            num_layers=num_layers_mlp,
            activation=act_mlp,
            activation_out=activation_out,
            dropout_rate=dropout_mlp,
        )

    def forward(self, data):
        x, edge_index = data.x, data.edge_index

        for conv, act,norm in zip(self.convs, self.conv_act,self.conv_norm):
            x=conv(x, edge_index)
            if self.gnn_norm==True:
                x=norm(x)
            x=act(x)

        # readout layer
        if self.readout_layer=='set2set':
            x=self.s2s(x,data.batch)
        if self.readout_layer=='all':
            x=aggr.MultiAggregation(['max','mean','std','min'])(x,data.batch)
        if self.readout_layer=='max':
            x=aggr.MultiAggregation(['max'])(x,data.batch)

        # classifier
        x =self.mlp(x)
        return x


class BaseGCNv3(torch.nn.Module):
    def __init__(
        self,
        in_node_channels,
        in_edge_channels,
        dim_gnn_embedding,
        num_gnn_layers,
        act_gnn,
        gnn_norm,
        dropout_gnn,
        readout_layer,
        processing_steps_readout,
        hidden_size_mlp,
        num_layers_mlp,
        dropout_mlp,
        act_mlp,
        activation_out,
        num_classes,
    ):
        """Initialise Base GCN class

        Args:
            in_node_channels: dimension of node input feature
            in_edge_channels: dimension of edge input feature
            width_node_embedding: width of MLP for node/edges features embedding
            dim_features_embedding: dimension of node features/edges embedding
            num_features_node_embedding: number fo MLP layers for node/features embedding
            activation_embedding: activation function for node/edges embedding
            dropout_embedding: dropout rate for node/edge embedding
            dim_gnn_embedding: hidden dimension of GNN.
            num_layers_gnn: numebr of gnn layers
            act_gnn: activation function for gnn
            dropout_gnn: dropout used in gnn
            gnn_norm: applies graph normalization over individual graphs
            dim_gnn_embedding: size of the node gnn embeddings
            processing_steps_readout:number of set2set lstm iterations
            readout_layer: type of readoutlayer
            num_layers_mlp: number of Linear layers for classification .
            hidden_size_mlp: width of classification MLP.
            num_classes: number of classes in output
            dropout_mlp: dropout rate of classification MLP.
            act_mlp: name of the activation for the mlp
        """    
        super().__init__()

        self.convs=ModuleList()
        self.gnn_norm=gnn_norm
        self.conv_norm = ModuleList()
        self.conv_act = ModuleList()
        for i in range(num_gnn_layers):
            dim_in = in_node_channels if i == 0 else dim_gnn_embedding
            conv = GCNConv(in_channels=dim_in, out_channels=dim_gnn_embedding)
            self.convs.append(conv)
            self.conv_norm.append(GraphNorm(in_channels=dim_gnn_embedding))
            self.conv_act.append(get_activation(act_gnn))

        self.readout_layer=readout_layer

        if self.readout_layer=='set2set':
            dim_in_mlp=dim_gnn_embedding*2
            self.s2s=Set2Set(in_channels=dim_gnn_embedding,processing_steps=processing_steps_readout)
        if self.readout_layer=='all':
            dim_in_mlp=dim_gnn_embedding*4
        if self.readout_layer=='max':
            dim_in_mlp=dim_gnn_embedding

        self.mlp = build_mlp(
            dim_in=dim_in_mlp,
            hidden_size_mlp=hidden_size_mlp,
            dim_out=num_classes,
            num_layers=num_layers_mlp,
            activation=act_mlp,
            activation_out=activation_out,
            dropout_rate=dropout_mlp,
        )

    def forward(self, data):
        x, edge_index,edge_attr = data.x, data.edge_index,data.edge_attr
        # print(edge_attr.size(),edge_attr,torch.sum(edge_attr,dim=1))
        for conv, act,norm in zip(self.convs, self.conv_act,self.conv_norm):
            x=conv(x, edge_index,edge_weight=torch.sum(edge_attr,dim=1))
            if self.gnn_norm==True:
                x=norm(x)
            x=act(x)

        # readout layer
        if self.readout_layer=='set2set':
            x=self.s2s(x,data.batch)
        if self.readout_layer=='all':
            x=aggr.MultiAggregation(['max','mean','std','min'])(x,data.batch)
        if self.readout_layer=='max':
            x=aggr.MultiAggregation(['max'])(x,data.batch)

        # classifier
        x =self.mlp(x)
        return x        


class BaseTransformer(torch.nn.Module):
    def __init__(
        self,
        in_node_channels,
        in_edge_channels,
        dim_gnn_embedding,
        num_gnn_layers,
        num_gnn_head,
        act_gnn,
        gnn_norm,
        dropout_gnn,
        readout_layer,
        processing_steps_readout,
        hidden_size_mlp,
        num_layers_mlp,
        dropout_mlp,
        act_mlp,
        activation_out,
        num_classes,
    ):
        """Initialise Base GCN class

        Args:
            in_node_channels: dimension of node input feature
            in_edge_channels: dimension of edge input feature
            dim_gnn_embedding: hidden dimension of GNN.
            num_layers_gnn: numebr of gnn layers
            num_gnn_head: number of gnn multi head attention
            act_gnn: activation function for gnn
            dropout_gnn: dropout used in gnn
            gnn_norm: applies graph normalization over individual graphs
            dim_gnn_embedding: size of the node gnn embeddings
            processing_steps_readout:number of set2set lstm iterations
            readout_layer: type of readoutlayer
            num_layers_mlp: number of Linear layers for classification .
            hidden_size_mlp: width of classification MLP.
            num_classes: number of classes in output
            dropout_mlp: dropout rate of classification MLP.
            act_mlp: name of the activation for the mlp
        """    
        super().__init__()
        self.convs=ModuleList()
        self.gnn_norm=gnn_norm
        self.conv_norm = ModuleList()
        self.conv_act = ModuleList()
        for i in range(num_gnn_layers):
            dim_in =  in_node_channels if i == 0 else dim_gnn_embedding*num_gnn_head
            if in_edge_channels!=0:

                conv = TransformerConv(in_channels=dim_in, out_channels=dim_gnn_embedding,edge_dim=in_edge_channels,heads=num_gnn_head)
            else:
                conv = TransformerConv(in_channels=dim_in, out_channels=dim_gnn_embedding,heads=num_gnn_head)
            self.convs.append(conv)
            self.conv_norm.append(GraphNorm(in_channels=dim_gnn_embedding))
            self.conv_act.append(get_activation(act_gnn))

        self.readout_layer=readout_layer
        self.s2s=Set2Set(in_channels=dim_gnn_embedding*num_gnn_head,processing_steps=processing_steps_readout)

        if self.readout_layer=='set2set':
            dim_in_mlp=dim_gnn_embedding*2*num_gnn_head
        if self.readout_layer=='all':
            dim_in_mlp=dim_gnn_embedding*4*num_gnn_head
        if self.readout_layer=='max':
            dim_in_mlp=dim_gnn_embedding*num_gnn_head

        self.mlp = build_mlp(
            dim_in=dim_in_mlp,
            hidden_size_mlp=hidden_size_mlp,
            dim_out=num_classes,
            num_layers=num_layers_mlp,
            activation=act_mlp,
            activation_out=activation_out,
            dropout_rate=dropout_mlp,
        )

    def forward(self, data):
        x, edge_index,edge_attr = data.x, data.edge_index,data.edge_attr
        for conv, act,norm in zip(self.convs, self.conv_act,self.conv_norm):
            if edge_attr is not None:
                x=conv(x, edge_index,edge_attr)
            else:
                x=conv(x,edge_index)            
            if self.gnn_norm==True:
                x=norm(x)
            x=act(x)

        # readout layer
        if self.readout_layer=='set2set':
            x=self.s2s(x,data.batch)
        if self.readout_layer=='all':
            x=aggr.MultiAggregation(['max','mean','std','min'])(x,data.batch)
        if self.readout_layer=='max':
            x=aggr.MultiAggregation(['max'])(x,data.batch)

        # emb = getattr(tg.nn,self.readout_layer)(x, data.batch)  # (batch, dim_emb)
        # classifier
        x =self.mlp(x)
        return x

class BaseGATv2(torch.nn.Module):
    def __init__(
        self,
        in_node_channels,
        in_edge_channels,
        dim_gnn_embedding,
        num_gnn_layers,
        num_gnn_head,
        act_gnn,
        gnn_norm,
        dropout_gnn,
        readout_layer,
        processing_steps_readout,
        hidden_size_mlp,
        num_layers_mlp,
        dropout_mlp,
        act_mlp,
        activation_out,
        num_classes,
    ):
        """Initialise Base GCN class

        Args:
            in_node_channels: dimension of node input feature
            in_edge_channels: dimension of edge input feature
            dim_gnn_embedding: hidden dimension of GNN.
            num_layers_gnn: numebr of gnn layers
            num_gnn_head: number of gnn multi head attention
            act_gnn: activation function for gnn
            dropout_gnn: dropout used in gnn
            gnn_norm: applies graph normalization over individual graphs
            dim_gnn_embedding: size of the node gnn embeddings
            processing_steps_readout:number of set2set lstm iterations
            readout_layer: type of readoutlayer
            num_layers_mlp: number of Linear layers for classification .
            hidden_size_mlp: width of classification MLP.
            num_classes: number of classes in output
            dropout_mlp: dropout rate of classification MLP.
            act_mlp: name of the activation for the mlp
        """    
        super().__init__()
        self.convs=ModuleList()
        self.gnn_norm=gnn_norm
        self.conv_norm = ModuleList()
        self.conv_act = ModuleList()
        for i in range(num_gnn_layers):
            dim_in =  in_node_channels if i == 0 else dim_gnn_embedding*num_gnn_head
            if in_edge_channels!=0:

                conv = GATv2Conv(in_channels=dim_in, out_channels=dim_gnn_embedding,edge_dim=in_edge_channels,heads=num_gnn_head)
            else:
                conv = GATv2Conv(in_channels=dim_in, out_channels=dim_gnn_embedding,heads=num_gnn_head)
            self.convs.append(conv)
            self.conv_norm.append(GraphNorm(in_channels=dim_gnn_embedding))
            self.conv_act.append(get_activation(act_gnn))

        self.readout_layer=readout_layer
        self.s2s=Set2Set(in_channels=dim_gnn_embedding*num_gnn_head,processing_steps=processing_steps_readout)

        if self.readout_layer=='set2set':
            dim_in_mlp=dim_gnn_embedding*2*num_gnn_head
        if self.readout_layer=='all':
            dim_in_mlp=dim_gnn_embedding*4*num_gnn_head
        if self.readout_layer=='max':
            dim_in_mlp=dim_gnn_embedding*num_gnn_head

        self.mlp = build_mlp(
            dim_in=dim_in_mlp,
            hidden_size_mlp=hidden_size_mlp,
            dim_out=num_classes,
            num_layers=num_layers_mlp,
            activation=act_mlp,
            activation_out=activation_out,
            dropout_rate=dropout_mlp,
        )

    def forward(self, data):
        x, edge_index,edge_attr = data.x, data.edge_index,data.edge_attr
        for conv, act,norm in zip(self.convs, self.conv_act,self.conv_norm):
            if edge_attr is not None:
                x=conv(x, edge_index,edge_attr)
            else:
                x=conv(x,edge_index)            
            if self.gnn_norm==True:
                x=norm(x)
            x=act(x)

        # readout layer
        if self.readout_layer=='set2set':
            x=self.s2s(x,data.batch)
        if self.readout_layer=='all':
            x=aggr.MultiAggregation(['max','mean','std','min'])(x,data.batch)
        if self.readout_layer=='max':
            x=aggr.MultiAggregation(['max'])(x,data.batch)

        # emb = getattr(tg.nn,self.readout_layer)(x, data.batch)  # (batch, dim_emb)
        # classifier
        x =self.mlp(x)
        return x


import itertools
class BaseGATv3(torch.nn.Module):
    def __init__(
        self,
        in_node_channels,
        in_edge_channels,
        dim_gnn_embedding,
        num_gnn_layers,
        num_gnn_head,
        act_gnn,
        gnn_norm,
        dropout_gnn,
        readout_layer,
        processing_steps_readout,
        hidden_size_mlp,
        num_layers_mlp,
        dropout_mlp,
        act_mlp,
        activation_out,
        num_classes,
    ):
        """Initialise Base GCN class

        Args:
            in_node_channels: dimension of node input feature
            in_edge_channels: dimension of edge input feature
            dim_gnn_embedding: hidden dimension of GNN.
            num_layers_gnn: numebr of gnn layers
            num_gnn_head: number of gnn multi head attention
            act_gnn: activation function for gnn
            dropout_gnn: dropout used in gnn
            gnn_norm: applies graph normalization over individual graphs
            dim_gnn_embedding: size of the node gnn embeddings
            processing_steps_readout:number of set2set lstm iterations
            readout_layer: type of readoutlayer
            num_layers_mlp: number of Linear layers for classification .
            hidden_size_mlp: width of classification MLP.
            num_classes: number of classes in output
            dropout_mlp: dropout rate of classification MLP.
            act_mlp: name of the activation for the mlp
        """    
        super().__init__()
        self.convs=ModuleList()
        self.gnn_norm=gnn_norm
        self.conv_norm = ModuleList()
        self.conv_act = ModuleList()
        for i in range(num_gnn_layers):
            dim_in =  in_node_channels if i == 0 else dim_gnn_embedding*num_gnn_head
            if in_edge_channels!=0:

                conv = GATv2Conv(in_channels=dim_in, out_channels=dim_gnn_embedding,edge_dim=in_edge_channels,heads=num_gnn_head)
            else:
                conv = GATv2Conv(in_channels=dim_in, out_channels=dim_gnn_embedding,heads=num_gnn_head)
            self.convs.append(conv)
            self.conv_norm.append(GraphNorm(in_channels=dim_gnn_embedding))
            self.conv_act.append(get_activation(act_gnn))

        self.readout_layer=readout_layer
        self.s2s=Set2Set(in_channels=dim_gnn_embedding*num_gnn_head,processing_steps=processing_steps_readout)


        dim_in_mlp=dim_gnn_embedding*num_gnn_head*4


        self.mlp = build_mlp(
            dim_in=dim_in_mlp,
            hidden_size_mlp=hidden_size_mlp,
            dim_out=num_classes,
            num_layers=num_layers_mlp,
            activation=act_mlp,
            activation_out=activation_out,
            dropout_rate=dropout_mlp,
        )

    def forward(self, data):
        x, edge_index,edge_attr = data.x, data.edge_index,data.edge_attr
        for conv, act,norm in zip(self.convs, self.conv_act,self.conv_norm):
            if edge_attr is not None:
                x=conv(x, edge_index,edge_attr)
            else:
                x=conv(x,edge_index)            
            if self.gnn_norm==True:
                x=norm(x)
            x=act(x)

        # readout layer
        # print(data.x,data.x.size(),'before')
        center_aug=30
        device_id=x.get_device()
        rows_to_keep=list(itertools.chain.from_iterable([list(range(30-10+75*i,center_aug+10+75*i)) for i in range(0, data.num_graphs)]))
        after=x[rows_to_keep,:].to(f"cuda:{device_id}")
        after_batch=torch.Tensor(list(itertools.chain.from_iterable([[i]*20 for i in range(0, data.num_graphs)]))).to(torch.long).to(f"cuda:{device_id}")
        x=aggr.MultiAggregation(['max','mean','std','min'])(after,after_batch)

        x =self.mlp(x)
        return x


class BaseGATv4(torch.nn.Module):
    def __init__(
        self,
        in_node_channels,
        in_edge_channels,
        dim_gnn_embedding,
        num_gnn_layers,
        num_gnn_head,
        act_gnn,
        gnn_norm,
        dropout_gnn,
        readout_layer,
        processing_steps_readout,
        hidden_size_mlp,
        num_layers_mlp,
        dropout_mlp,
        act_mlp,
        activation_out,
        num_classes,
    ):
        """Initialise Base GCN class

        Args:
            in_node_channels: dimension of node input feature
            in_edge_channels: dimension of edge input feature
            dim_gnn_embedding: hidden dimension of GNN.
            num_layers_gnn: numebr of gnn layers
            num_gnn_head: number of gnn multi head attention
            act_gnn: activation function for gnn
            dropout_gnn: dropout used in gnn
            gnn_norm: applies graph normalization over individual graphs
            dim_gnn_embedding: size of the node gnn embeddings
            processing_steps_readout:number of set2set lstm iterations
            readout_layer: type of readoutlayer
            num_layers_mlp: number of Linear layers for classification .
            hidden_size_mlp: width of classification MLP.
            num_classes: number of classes in output
            dropout_mlp: dropout rate of classification MLP.
            act_mlp: name of the activation for the mlp
        """    
        super().__init__()
        self.convs=ModuleList()
        self.gnn_norm=gnn_norm
        self.conv_norm = ModuleList()
        self.conv_act = ModuleList()
        for i in range(num_gnn_layers):
            dim_in =  in_node_channels if i == 0 else dim_gnn_embedding*num_gnn_head
            if in_edge_channels!=0:

                conv = GATv2Conv(in_channels=dim_in, out_channels=dim_gnn_embedding,edge_dim=in_edge_channels,heads=num_gnn_head)
            else:
                conv = GATv2Conv(in_channels=dim_in, out_channels=dim_gnn_embedding,heads=num_gnn_head)
            self.convs.append(conv)
            self.conv_norm.append(GraphNorm(in_channels=dim_gnn_embedding))
            self.conv_act.append(get_activation(act_gnn))

        self.readout_layer=readout_layer
        self.s2s=Set2Set(in_channels=dim_gnn_embedding*num_gnn_head,processing_steps=processing_steps_readout)


        dim_in_mlp=dim_gnn_embedding*num_gnn_head*4


        self.mlp = build_mlp(
            dim_in=dim_in_mlp,
            hidden_size_mlp=hidden_size_mlp,
            dim_out=num_classes,
            num_layers=num_layers_mlp,
            activation=act_mlp,
            activation_out=activation_out,
            dropout_rate=dropout_mlp,
        )

    def forward(self, data):
        x, edge_index,edge_attr = data.x, data.edge_index,data.edge_attr
        for conv, act,norm in zip(self.convs, self.conv_act,self.conv_norm):
            if edge_attr is not None:
                x=conv(x, edge_index,edge_attr)
            else:
                x=conv(x,edge_index)            
            if self.gnn_norm==True:
                x=norm(x)
            x=act(x)

        # readout layer
        # print(data.x,data.x.size(),'before')
        center_aug=30
        device_id=x.get_device()
        rows_to_keep=list(itertools.chain.from_iterable([list(range(center_aug-15+75*i,center_aug+15+75*i)) for i in range(0, data.num_graphs)]))
        after=x[rows_to_keep,:].to(f"cuda:{device_id}")
        after_batch=torch.Tensor(list(itertools.chain.from_iterable([[i]*30 for i in range(0, data.num_graphs)]))).to(torch.long).to(f"cuda:{device_id}")
        x=aggr.MultiAggregation(['max','mean','std','min'])(after,after_batch)

        x =self.mlp(x)
        return x


class BaseGATv5(torch.nn.Module):
    def __init__(
        self,
        in_node_channels,
        in_edge_channels,
        dim_gnn_embedding,
        num_gnn_layers,
        num_gnn_head,
        act_gnn,
        gnn_norm,
        dropout_gnn,
        readout_layer,
        processing_steps_readout,
        hidden_size_mlp,
        num_layers_mlp,
        dropout_mlp,
        act_mlp,
        activation_out,
        num_classes,
    ):
        """Initialise Base GCN class

        Args:
            in_node_channels: dimension of node input feature
            in_edge_channels: dimension of edge input feature
            dim_gnn_embedding: hidden dimension of GNN.
            num_layers_gnn: numebr of gnn layers
            num_gnn_head: number of gnn multi head attention
            act_gnn: activation function for gnn
            dropout_gnn: dropout used in gnn
            gnn_norm: applies graph normalization over individual graphs
            dim_gnn_embedding: size of the node gnn embeddings
            processing_steps_readout:number of set2set lstm iterations
            readout_layer: type of readoutlayer
            num_layers_mlp: number of Linear layers for classification .
            hidden_size_mlp: width of classification MLP.
            num_classes: number of classes in output
            dropout_mlp: dropout rate of classification MLP.
            act_mlp: name of the activation for the mlp
        """    
        super().__init__()
        self.convs=ModuleList()
        self.gnn_norm=gnn_norm
        self.conv_norm = ModuleList()
        self.conv_act = ModuleList()
        for i in range(num_gnn_layers):
            dim_in =  in_node_channels if i == 0 else dim_gnn_embedding*num_gnn_head
            if in_edge_channels!=0:

                conv = GATv2Conv(in_channels=dim_in, out_channels=dim_gnn_embedding,edge_dim=in_edge_channels,heads=num_gnn_head)
            else:
                conv = GATv2Conv(in_channels=dim_in, out_channels=dim_gnn_embedding,heads=num_gnn_head)
            self.convs.append(conv)
            self.conv_norm.append(GraphNorm(in_channels=dim_gnn_embedding))
            self.conv_act.append(get_activation(act_gnn))

        self.readout_layer=readout_layer
        # self.s2s=Set2Set(in_channels=dim_gnn_embedding*num_gnn_head,processing_steps=processing_steps_readout)


        dim_in_mlp=dim_gnn_embedding*num_gnn_head


        self.mlp = build_mlp(
            dim_in=dim_in_mlp,
            hidden_size_mlp=hidden_size_mlp,
            dim_out=num_classes,
            num_layers=num_layers_mlp,
            activation=act_mlp,
            activation_out=activation_out,
            dropout_rate=dropout_mlp,
        )

    def forward(self, data):
        x, edge_index,edge_attr = data.x, data.edge_index,data.edge_attr
        for conv, act,norm in zip(self.convs, self.conv_act,self.conv_norm):
            if edge_attr is not None:
                x=conv(x, edge_index,edge_attr)
            else:
                x=conv(x,edge_index)            
            if self.gnn_norm==True:
                x=norm(x)
            x=act(x)

        # readout layer
        # print(data.x,data.x.size(),'before')
        device_id=x.get_device()
        center_aug=30
        rows_to_keep=list(itertools.chain.from_iterable([list(range(center_aug-20+75*i,center_aug+20+75*i)) for i in range(0, data.num_graphs)]))
        after=x[rows_to_keep,:].to(f"cuda:{device_id}")
        after_batch=torch.Tensor(list(itertools.chain.from_iterable([[i]*40 for i in range(0, data.num_graphs)]))).to(torch.long).to(f"cuda:{device_id}")
        # x=aggr.MultiAggregation(['max','mean','std','min'])(after,after_batch)
        x=aggr.MultiAggregation(['max'])(after,after_batch)

        x =self.mlp(x)
        return x



class BaseGATvX(torch.nn.Module):
    def __init__(
        self,
        in_node_channels,
        in_edge_channels,
        dim_gnn_embedding,
        num_gnn_layers,
        num_gnn_head,
        act_gnn,
        gnn_norm,
        dropout_gnn,
        readout_layer,
        processing_steps_readout,
        hidden_size_mlp,
        num_layers_mlp,
        dropout_mlp,
        act_mlp,
        activation_out,
        num_classes,
        window,
    ):
        """Initialise Base GCN class

        Args:
            in_node_channels: dimension of node input feature
            in_edge_channels: dimension of edge input feature
            dim_gnn_embedding: hidden dimension of GNN.
            num_layers_gnn: numebr of gnn layers
            num_gnn_head: number of gnn multi head attention
            act_gnn: activation function for gnn
            dropout_gnn: dropout used in gnn
            gnn_norm: applies graph normalization over individual graphs
            dim_gnn_embedding: size of the node gnn embeddings
            processing_steps_readout:number of set2set lstm iterations
            readout_layer: type of readoutlayer
            num_layers_mlp: number of Linear layers for classification .
            hidden_size_mlp: width of classification MLP.
            num_classes: number of classes in output
            dropout_mlp: dropout rate of classification MLP.
            act_mlp: name of the activation for the mlp
        """    
        super().__init__()
        self.convs=ModuleList()
        self.gnn_norm=gnn_norm
        self.conv_norm = ModuleList()
        self.conv_act = ModuleList()
        for i in range(num_gnn_layers):
            dim_in =  in_node_channels if i == 0 else dim_gnn_embedding*num_gnn_head
            if in_edge_channels!=0:

                conv = GATv2Conv(in_channels=dim_in, out_channels=dim_gnn_embedding,edge_dim=in_edge_channels,heads=num_gnn_head)
            else:
                conv = GATv2Conv(in_channels=dim_in, out_channels=dim_gnn_embedding,heads=num_gnn_head)
            self.convs.append(conv)
            self.conv_norm.append(GraphNorm(in_channels=dim_gnn_embedding))
            self.conv_act.append(get_activation(act_gnn))

        self.readout_layer=readout_layer
        # self.s2s=Set2Set(in_channels=dim_gnn_embedding*num_gnn_head,processing_steps=processing_steps_readout)
        self.vx=window

        dim_in_mlp=dim_gnn_embedding*num_gnn_head


        self.mlp = build_mlp(
            dim_in=dim_in_mlp,
            hidden_size_mlp=hidden_size_mlp,
            dim_out=num_classes,
            num_layers=num_layers_mlp,
            activation=act_mlp,
            activation_out=activation_out,
            dropout_rate=dropout_mlp,
        )

    def forward(self, data):
        x, edge_index,edge_attr = data.x, data.edge_index,data.edge_attr
        for conv, act,norm in zip(self.convs, self.conv_act,self.conv_norm):
            if edge_attr is not None:
                x=conv(x, edge_index,edge_attr)
            else:
                x=conv(x,edge_index)            
            if self.gnn_norm==True:
                x=norm(x)
            x=act(x)

        # readout layer
        # print(data.x,data.x.size(),'before')
        device_id=x.get_device()
        center_aug=30
        vx=self.vx
        rows_to_keep=list(itertools.chain.from_iterable([list(range(center_aug-vx+75*i,center_aug+vx+75*i)) for i in range(0, data.num_graphs)]))
        after=x[rows_to_keep,:].to(f"cuda:{device_id}")
        after_batch=torch.Tensor(list(itertools.chain.from_iterable([[i]*2*vx for i in range(0, data.num_graphs)]))).to(torch.long).to(f"cuda:{device_id}")
        # x=aggr.MultiAggregation(['max','mean','std','min'])(after,after_batch)
        x=aggr.MultiAggregation(['max'])(after,after_batch)

        x =self.mlp(x)
        return x


class BaseGATv6(torch.nn.Module):
    def __init__(
        self,
        in_node_channels,
        in_edge_channels,
        dim_gnn_embedding,
        num_gnn_layers,
        num_gnn_head,
        act_gnn,
        gnn_norm,
        dropout_gnn,
        readout_layer,
        processing_steps_readout,
        hidden_size_mlp,
        num_layers_mlp,
        dropout_mlp,
        act_mlp,
        activation_out,
        num_classes,
    ):
        """Initialise Base GCN class

        Args:
            in_node_channels: dimension of node input feature
            in_edge_channels: dimension of edge input feature
            dim_gnn_embedding: hidden dimension of GNN.
            num_layers_gnn: numebr of gnn layers
            num_gnn_head: number of gnn multi head attention
            act_gnn: activation function for gnn
            dropout_gnn: dropout used in gnn
            gnn_norm: applies graph normalization over individual graphs
            dim_gnn_embedding: size of the node gnn embeddings
            processing_steps_readout:number of set2set lstm iterations
            readout_layer: type of readoutlayer
            num_layers_mlp: number of Linear layers for classification .
            hidden_size_mlp: width of classification MLP.
            num_classes: number of classes in output
            dropout_mlp: dropout rate of classification MLP.
            act_mlp: name of the activation for the mlp
        """    
        super().__init__()
        self.convs=ModuleList()
        self.gnn_norm=gnn_norm
        self.conv_norm = ModuleList()
        self.conv_act = ModuleList()
        for i in range(num_gnn_layers):
            dim_in =  in_node_channels if i == 0 else dim_gnn_embedding*num_gnn_head
            if in_edge_channels!=0:

                conv = GATv2Conv(in_channels=dim_in, out_channels=dim_gnn_embedding,edge_dim=in_edge_channels,heads=num_gnn_head)
            else:
                conv = GATv2Conv(in_channels=dim_in, out_channels=dim_gnn_embedding,heads=num_gnn_head)
            self.convs.append(conv)
            self.conv_norm.append(GraphNorm(in_channels=dim_gnn_embedding))
            self.conv_act.append(get_activation(act_gnn))

        self.readout_layer=readout_layer
        self.s2s=Set2Set(in_channels=dim_gnn_embedding*num_gnn_head,processing_steps=processing_steps_readout)


        dim_in_mlp=dim_gnn_embedding*num_gnn_head*4


        self.mlp = build_mlp(
            dim_in=dim_in_mlp,
            hidden_size_mlp=hidden_size_mlp,
            dim_out=num_classes,
            num_layers=num_layers_mlp,
            activation=act_mlp,
            activation_out=activation_out,
            dropout_rate=dropout_mlp,
        )

    def forward(self, data):
        x, edge_index,edge_attr = data.x, data.edge_index,data.edge_attr
        for conv, act,norm in zip(self.convs, self.conv_act,self.conv_norm):
            if edge_attr is not None:
                x=conv(x, edge_index,edge_attr)
            else:
                x=conv(x,edge_index)            
            if self.gnn_norm==True:
                x=norm(x)
            x=act(x)

        # readout layer
        # print(data.x,data.x.size(),'before')
        device_id=x.get_device()
        center_aug=30
        rows_to_keep=list(itertools.chain.from_iterable([list(range(center_aug-25+75*i,center_aug+25+75*i)) for i in range(0, data.num_graphs)]))
        after=x[rows_to_keep,:].to(f"cuda:{device_id}")
        after_batch=torch.Tensor(list(itertools.chain.from_iterable([[i]*50 for i in range(0, data.num_graphs)]))).to(torch.long).to(f"cuda:{device_id}")
        x=aggr.MultiAggregation(['max','mean','std','min'])(after,after_batch)

        x =self.mlp(x)
        return x



class BaseNNConv(torch.nn.Module):
    """An edge-conditioned graph convolutional network followed by linear layers."""

    def __init__(
        self,
        in_node_channels,
        in_edge_channels,
        dim_gnn_embedding,
        num_gnn_layers,
        num_gnn_head,
        act_gnn,
        gnn_norm,
        dropout_gnn,
        readout_layer,
        processing_steps_readout,
        hidden_size_mlp,
        num_layers_mlp,
        dropout_mlp,
        act_mlp,
        activation_out,
        num_classes,
        num_layers=2
    ):
        """Initialise Base NNconv class

        Args:
            in_node_channels: dimension of node input feature
            in_edge_channels: dimension of edge input feature
            dim_gnn_embedding: hidden dimension of GNN.
            num_layers_gnn: numebr of gnn layers
            num_gnn_head: number of gnn multi head attention
            act_gnn: activation function for gnn
            dropout_gnn: dropout used in gnn
            gnn_norm: applies graph normalization over individual graphs
            dim_gnn_embedding: size of the node gnn embeddings
            processing_steps_readout:number of set2set lstm iterations
            readout_layer: type of readoutlayer
            num_layers_mlp: number of Linear layers for classification .
            hidden_size_mlp: width of classification MLP.
            num_classes: number of classes in output
            dropout_mlp: dropout rate of classification MLP.
            act_mlp: name of the activation for the mlp
        """  
        super().__init__()

        convs = []
        for i in range(num_layers):
            dim_in = in_node_channels if i == 0 else dim_gnn_embedding
            mlp = build_mlp(
                dim_in=in_edge_channels,
                hidden_size_mlp=dim_gnn_embedding,
                dim_out=dim_gnn_embedding*dim_in,
                num_layers=2,
                activation=act_gnn,
                activation_out=act_gnn,
                dropout_rate=dropout_gnn,
            )
            conv = NNConv(dim_in, dim_gnn_embedding, mlp, aggr="mean")
            convs.append(conv)
        self.convs = nn.ModuleList(convs)

        self.conv_acts = nn.ModuleList([get_activation(act_gnn) for _ in range(num_layers)])

        self.readout_layer=readout_layer

        self.s2s=Set2Set(in_channels=dim_gnn_embedding*num_gnn_head,processing_steps=processing_steps_readout)

        if self.readout_layer=='set2set':
            dim_in_mlp=dim_gnn_embedding*2
        if self.readout_layer=='all':
            dim_in_mlp=dim_gnn_embedding*4
        if self.readout_layer=='max':
            dim_in_mlp=dim_gnn_embedding


        self.mlp = build_mlp(
            dim_in=dim_in_mlp,
            hidden_size_mlp=hidden_size_mlp,
            dim_out=num_classes,
            num_layers=num_layers_mlp,
            activation=act_mlp,
            activation_out=activation_out,
            dropout_rate=dropout_mlp,
        )


    def forward(self, data):
        """Forward pass through the GCN.

        Args:
            data: input data with node and edge features.

        Returns:
            logits: unnormalized logits, shape = (batch, num_classes)
            emd: embedding of graph, shape = (batch, dim_emb)
        """
        # graph conv
        x = data.x
        edge_index = data.edge_index
        edge_attr = data.edge_attr
        batch = data.batch
        for conv, act in zip(self.convs, self.conv_acts):
            # print('hey',x.size(),edge_attr.size())
            x = conv(x, edge_index, edge_attr)
            x = act(x)
            

        # readout layer
        if self.readout_layer=='set2set':
            x=self.s2s(x)
        if self.readout_layer=='all':
            x=aggr.MultiAggregation(['max','mean','std','min'])(x,data.batch)
        if self.readout_layer=='mean':
            x=aggr.MultiAggregation(['mean'])(x,data.batch)

        # print( torch.isnan(x))

        # classifier
        x = self.mlp(x)
        # print(x)
        return x

class BasePNA(nn.Module):
    """A PNA graph convolution operator from:
    https://pytorch-geometric.readthedocs.io/en/latest/modules/nn.html#torch_geometric.nn.conv.PNAConv
    """
    def __init__(self,
        in_node_channels,
        in_edge_channels,
        dim_gnn_embedding,
        num_gnn_layers,
        num_gnn_head,
        act_gnn,
        gnn_norm,
        dropout_gnn,
        readout_layer,
        processing_steps_readout,
        hidden_size_mlp,
        num_layers_mlp,
        dropout_mlp,
        act_mlp,
        activation_out,
        num_classes,
        deg,
    ):
        """Initialise Base GCN class

        Args:
            in_node_channels: dimension of node input feature
            in_edge_channels: dimension of edge input feature
            dim_gnn_embedding: hidden dimension of GNN.
            num_layers_gnn: numebr of gnn layers
            num_gnn_head: number of gnn multi head attention
            act_gnn: activation function for gnn
            dropout_gnn: dropout used in gnn
            gnn_norm: applies graph normalization over individual graphs
            dim_gnn_embedding: size of the node gnn embeddings
            processing_steps_readout:number of set2set lstm iterations
            readout_layer: type of readoutlayer
            num_layers_mlp: number of Linear layers for classification .
            hidden_size_mlp: width of classification MLP.
            num_classes: number of classes in output
            dropout_mlp: dropout rate of classification MLP.
            act_mlp: name of the activation for the mlp
            deg:compute in-degree histogram tensor
        """     
        super().__init__()


        aggregators = ['mean', 'min', 'max', 'std']
        scalers = ['identity', 'amplification', 'attenuation']

        self.convs=ModuleList()
        self.gnn_norm=gnn_norm
        self.conv_norm = ModuleList()
        self.conv_act = ModuleList()
        for i in range(num_gnn_layers):
            dim_in = in_node_channels if i == 0 else dim_gnn_embedding
            conv = PNAConv(in_channels=dim_in, out_channels=dim_gnn_embedding,
                           aggregators=aggregators, scalers=scalers, deg=deg,
                           edge_dim=in_edge_channels, towers=4, pre_layers=1, post_layers=1,
                           divide_input=False)
            self.convs.append(conv)
            self.conv_norm.append(GraphNorm(in_channels=dim_gnn_embedding))
            self.conv_act.append(get_activation(act_gnn))

        self.readout_layer=readout_layer
        self.s2s=Set2Set(in_channels=dim_gnn_embedding,processing_steps=processing_steps_readout)

        if self.readout_layer=='set2set':
            dim_in_mlp=dim_gnn_embedding*2
        if self.readout_layer=='all':
            dim_in_mlp=dim_gnn_embedding*4
        if self.readout_layer=='max':
            dim_in_mlp=dim_gnn_embedding

        self.mlp = build_mlp(
            dim_in=dim_in_mlp,
            hidden_size_mlp=hidden_size_mlp,
            dim_out=num_classes,
            num_layers=num_layers_mlp,
            activation=act_mlp,
            activation_out="",
            dropout_rate=dropout_mlp,
        )

    def forward(self, data):
        x, edge_index,edge_attr = data.x, data.edge_index,data.edge_attr
        for conv, act,norm in zip(self.convs, self.conv_act,self.conv_norm):
            if edge_attr is not None:
                x=conv(x, edge_index,edge_attr)
            else:
                x=conv(x,edge_index)            
            if self.gnn_norm==True:
                x=norm(x)
            x=act(x)

        # readout layer
        if self.readout_layer=='set2set':
            x=self.s2s(x,data.batch)
        if self.readout_layer=='all':
            x=aggr.MultiAggregation(['max','mean','std','min'])(x,data.batch)
        if self.readout_layer=='max':
            x=aggr.MultiAggregation(['max'])(x,data.batch)

        # classifier
        x =self.mlp(x)
        return x


class BinningLayer(nn.Module):
    def __init__(self, bins=8, f_max=1e4):
        super().__init__()
        self.bins = bins
        self.fluo_boundary = torch.logspace(
            start=0, end=np.log10(f_max), steps=self.bins + 1, device=device
        )
        self.fluo_boundary[0] = 0
        self.fluo_boundary = torch.reshape(self.fluo_boundary, (-1, 1))

    def forward(self, X):
        distribution = torch.distributions.normal.Normal(loc=X[:, 0], scale=X[:, 1])
        # torch.transpose(distribution.cdf(self.fluo_boundary),0,1)
        temp = torch.ones(X.size(dim=0), 1, device=device)
        return torch.diff(
            torch.transpose(distribution.cdf(self.fluo_boundary), 0, 1), dim=-1
        )  # Compute first forward difference along the last dimension






# layer = BinningLayer(4,10)
# normal_tensor=torch.tensor([[0.0,1.0],[15.0,1.0],[3.0,2.0]])
# print(layer(normal_tensor))

# ex=torch.distributions.normal.Normal(loc=normal_tensor[0], scale=normal_tensor[1])
# print(ex.event_shape)
# print(ex.sample())
# print(ex.sample_n(3))
# print(ex.cdf(torch.tensor([[0],[1.2],[15]])))
# print(ex.cdf(torch.tensor([[0],[4],[12]])))

# torch.manual_seed(0)  #  for repeatable results
# basic_model = BasicModel()
# inp = np.array([[[[1,2,3,4],  # batch(=1) x channels(=1) x height x width
#                   [1,2,3,4],
#                   [1,2,3,4]]]])
# x = torch.tensor(inp, dtype=torch.float)
# print('Forward computation thru model:', basic_model(x))
