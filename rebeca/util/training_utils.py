"""Modules containing key training functions"""
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
import torch
from sklearn.metrics import confusion_matrix
from itertools import chain

def train_batch_seq(model, inputs, labels, loss_fn, optimizer,scaler):
    """Defines the training step after one forward pass of one batch of data
    for generic PyTorch models with accuracy ,metric"""
    for param in model.parameters():  # zero the parameter gradients
        param.grad = None
    # print('here is ',inputs,labels)
    # Enables autocasting for the forward pass (model + loss)
    with torch.cuda.amp.autocast():
        softmax = model(inputs)  # (batch, num_classes)
        # print('to verify',inputs.size(),softmax.size(),labels.size())
        # labels=labels.unsqueeze(1)
        loss = loss_fn(softmax, labels)
    #backward

    scaler.scale(loss).backward()
    scaler.step(optimizer)
    scaler.update()
    return float(loss.detach().cpu().numpy())



def train_batch(model, inputs, labels, loss_fn, optimizer,scaler):
    """Defines the training step after one forward pass of one batch of data
    for generic PyTorch models with accuracy ,metric"""
    for param in model.parameters():  # zero the parameter gradients
        param.grad = None
    # print('here is ',inputs,labels)
    # Enables autocasting for the forward pass (model + loss)
    with torch.cuda.amp.autocast():
        # print(inputs,inputs.x,inputs.edge_index)    
        softmax = model(inputs)  # (batch, num_classes)
        # print('to verify',inputs,softmax)
        labels=labels.unsqueeze(1)
        loss = loss_fn(softmax, labels)
    #backward

    scaler.scale(loss).backward()
    scaler.step(optimizer)
    scaler.update()
    return float(loss.detach().cpu().numpy())

# def train_graph(model, data, loss_fn, optimizer, device):
#     """Defines the training step after one forward pass of one batch of data
#     for PyTorch Geometric models"""
#     # zero the parameter gradients
#     optimizer.zero_grad()
#     # forward + backward + optimize
#     logits = model(data)  # (batch, num_classes)
#     loss_train = loss_fn(logits, data.y)
#     loss_train.backward()
#     optimizer.step()
#     return float(loss_train.detach().cpu().numpy())


# def eval_graph(model, data, loss_fn, device):
#     """Defines the evaluation step"""
#     logits = model(data)  # (batch, num_classes)
#     loss_val = loss_fn(logits, data.y)
#     return float(loss_val.detach().cpu().numpy())


def evaluate(data_loader, architecture, epoch, split, device):
    """Returns confusion matrix on validation set after each epoch."""
    y_p = []
    y_t = []
    x_s = []
    with torch.no_grad():
        for data in list(data_loader):
            inputs, labels, sequence = data
            inputs, labels = inputs.to(device), labels.to(device)  # Send input tensors to the GPU
            mode_bin = labels.argmax(dim=1)
            output = architecture(inputs).argmax(dim=1)
            y_t += mode_bin.tolist()
            y_p += output.tolist()
            x_s += list(sequence)
    return get_cm(y_p, y_t, x_s, epoch, split)


def get_cm(y_p, y_t, x_s, epoch, split):
    # Build confusion matrix
    fig, ax = plt.subplots(figsize=(10, 10))
    loss=np.mean([np.abs(y_p[i]-y_t[i]) for i in range(len(y_t))])

    # plt.figure(figsize = (12,7))
    sns.scatterplot(x=y_t,y=y_p,color='silver',linewidth=0.1,edgecolor='black',s=0.3)
    ax.set_title(
        f"Prediction vs ground truth at epoch {epoch} on the {split} set. L1 Loss:{loss}."
    )
    plt.ylim([0,1])
    return fig


def evaluate_graph(data_loader, architecture, epoch, split, device):
    """Returns confusion matrix on validation set after each epoch."""
    y_p = []
    y_t = []
    x_s = []
    with torch.no_grad():
        for data in list(data_loader):
            inputs, labels, sequence = data, data.y, data.sequence
            inputs, labels = inputs.to(device), labels.to(device)  # Send input tensors to the GPU
            output = torch.squeeze(architecture(inputs))
            y_t += labels.tolist()
            y_p += output.tolist()
            x_s += sequence
    return get_cm(y_p, y_t, x_s, epoch, split)



def val_batch(
    model,
    inputs,
    labels,
    loss_fn,
):
    """Validating step using a batch of data."""
    softmax = model(inputs)  # (batch, num_classes)
    labels=labels.unsqueeze(1)
    loss = loss_fn(softmax, labels)
    return float(loss.detach().cpu().numpy())

def val_batch_seq(
    model,
    inputs,
    labels,
    loss_fn,
):
    """Validating step using a batch of data."""
    softmax = model(inputs)  # (batch, num_classes)
    loss = loss_fn(softmax, labels)
    return float(loss.detach().cpu().numpy())


def save_checkpoint(architecture, optimizer, best_ckpt_path, epoch):
    """Save the model checkpoint."""
    state = {
        "epoch": epoch,
        "state_dict": architecture.state_dict(),
        "optimizer": optimizer.state_dict(),
    }
    torch.save(state, best_ckpt_path)
