"""Module for training neural network architectures."""
import os
import pprint
from pathlib import Path

import neptune.new as neptune
import numpy as np
import pandas as pd
import torch
import torch.nn as nn
import torch.optim as optim
from torch.utils.data import DataLoader
from torch.utils.data.sampler import SequentialSampler
from torch.optim.lr_scheduler import ReduceLROnPlateau
from tqdm import tqdm

import rebeca.data.dataset as da
import rebeca.model.sequence_model as sm
from rebeca.core import load_cfg, parse_args
from rebeca.data.dataset import GroupedBatchSampler, UTRDataset
from rebeca.util.loss_function import kl_divergence

# from rebeca.model.sequence_model import DoubleConv, init_weights,CONVBILSTM,REC,MLP
from rebeca.util.training_utils import evaluate, save_checkpoint, train_batch_seq, val_batch_seq


# Load args
args = parse_args()
# Load config.yml
cfg = load_cfg(args.config / "config.yml")

# Print selected configuration
pp = pprint.PrettyPrinter(indent=4)
pp.pprint(cfg)


def main():  # noqa: CCR001
    """Main script."""
    # Selecting hardware
    device = torch.device(cfg['GPU'] if torch.cuda.is_available() else "cpu")
    print(f"Using {device} device")

    # Reproducibility settings
    seed = 32
    np.random.seed(seed)
    torch.manual_seed(seed)
    torch.backends.cudnn.deterministic = True
    torch.backends.cudnn.benchmark = False  # switch on optimiser for cuDNN algorithms for computing convolutions, switch off for reproducibility

    # Experiment Tracking
    if args.neptune:
        run = neptune.init(
            project="pag/rebeca",
            api_token=os.environ["NEPTUNE_API_TOKEN"],
            capture_stdout=False,
            capture_stderr=False,
        )
        with open(Path(args.config) / "neptune_run.txt", mode="w") as f:
            f.write(f"The url for the neptune run is {run.get_run_url()}")

    # Load architecture
    model = getattr(sm, cfg["architecture"]["name"])(**cfg["param_architecture"])
    model.apply(sm.init_weights)
    print("the model is", model)
    nb_param = sum(
        p.numel() for p in model.parameters() if p.requires_grad
    )  # get the number of trainable parameters.
    print(f"the model has {nb_param} parameters.")

    # Specifying hardware
    if torch.cuda.is_available():
        # if torch.cuda.device_count() > 1:
        #     model = nn.DataParallel(model)
        model.to(device)  # you need to send your model weights to the GPU
        num_workers = cfg["train"]["num_workers"] * torch.cuda.device_count()
        batch_size = cfg["train"]["batch_size"] * torch.cuda.device_count()
    else:
        num_workers = 0
        batch_size = cfg["train"]["batch_size"]

    # Load data
    train_dataloader = DataLoader(
        getattr(da, cfg["dataset"])(split="train", **cfg["param_dataset"]),
        batch_size=batch_size,
        shuffle=True,
        num_workers=num_workers,
        pin_memory=True,
    )
    val_dataloader = DataLoader(
        getattr(da, cfg["dataset"])(split="val", **cfg["param_dataset"]),
        batch_size=batch_size,
        shuffle=True,
        num_workers=num_workers,
        pin_memory=True,
    )

    # else:
    #     dataset = UTRDataset(journal=f"{args.data_name}", split="train", utr_max_length=30)
    #     idx_len=list(pd.read_csv('rebeca/data/nar_np_train.csv')['UTR'].str.len())
    #     sampler=SequentialSampler(dataset)
    #     cu_batch_sampler=GroupedBatchSampler(sampler,idx_len,64)
    #     train_dataloader=DataLoader(dataset,batch_sampler=cu_batch_sampler)
    #
    #     dataset = UTRDataset(journal=f"{args.data_name}", split="val", utr_max_length=30)
    #     idx_len=list(pd.read_csv('rebeca/data/nar_np_val.csv')['UTR'].str.len())
    #     sampler=SequentialSampler(dataset)
    #     cu_batch_sampler=GroupedBatchSampler(sampler,idx_len,64)
    #     val_dataloader=DataLoader(dataset,batch_sampler=cu_batch_sampler)

    # Declare loss function and optimizer
    try: 
        loss_fn = getattr(nn, cfg["loss"]["name"])()
    except AttributeError:
        loss_fn=kl_divergence
    optimizer = getattr(optim, cfg["optimizer"]["name"])(
        model.parameters(), lr=float(cfg["optimizer"]["lr"])
    )

    scheduler = ReduceLROnPlateau(optimizer, mode='min', factor=0.5, patience=5,
                              min_lr=1e-5)

    scaler=torch.cuda.amp.GradScaler()

    best_loss = 100
    best_epoch = 0
    num_epochs, warmup_epochs, patience = (
        int(cfg["train"]["num_epochs"]),
        int(cfg["train"]["warmup_epochs"]),
        int(cfg["train"]["patience"]),
    )
    for epoch in range(num_epochs):  # loop over the dataset multiple times
        model.train()
        loss = []
        loss_val = []
        loop = tqdm(train_dataloader, total=int(len(train_dataloader)))
        for _, data in enumerate(loop):
            inputs, labels, _ = data  # get the inputs; data is a list of [inputs, labels]
            inputs, labels = inputs.to(device), labels.to(device)  # Send input tensors to the GPU
            loss_batch = train_batch_seq(model, inputs, labels, loss_fn, optimizer,scaler)
            loss.append(loss_batch)
            if args.neptune:
                # log on neptune
                run["train/batch/loss"].log(loss_batch)
                # run["train/batch/accuracy"].log(acc_batch)
        if args.neptune:
            # log on neptune
            run["train/batch/epoch_loss"].log(np.mean(loss))
            # run["train/batch/epoch_accuracy"].log(np.mean(acc))
        # update progress bar
        loop.set_postfix(loss=np.mean(loss))#, accuracy=np.mean(acc))
        loop.set_description(f"Epoch [{epoch}/{num_epochs}]")

        with torch.no_grad():
            model.eval()
            for data in val_dataloader:
                inputs, labels, _ = data
                inputs, labels = inputs.to(device), labels.to(
                    device
                )  # Send input tensors to the GPU
                loss_val_batch = val_batch_seq(
                    model, inputs, labels, loss_fn
                )
                loss_val.append(loss_val_batch)
                # if args.neptune:
                #     run["val/batch/accuracy"].log(acc_val_batch)
            val_loss_epoch = np.mean(loss_val)
            scheduler.step(val_loss_epoch)
            if args.neptune:
            # log on neptune
                run["val/batch/epoch_loss"].log(np.mean(val_loss_epoch))
            # if args.neptune:
            #     run["train/distribution"].log(
            #         evaluate(val_dataloader, model, epoch, "validation", device)
            #     )
                # run["val/batch/epoch_accuracy"].log(np.mean(acc_val_batch))
            if val_loss_epoch < best_loss and epoch > warmup_epochs:
                best_loss = val_loss_epoch
                best_epoch = epoch
                # keep snapshot
                best_model = model.state_dict()
        if epoch - best_epoch > patience:
            print("training finished")
            if args.neptune:
                # Log PyTorch model weights
                run["model_checkpoints/my_model"].upload(
                    f"model_checkpoints/best_model_epoch_{epoch}.pth"
                )
            break
    # At this point save a snapshot of the best model
    torch.save(best_model, Path(args.config) / f"best_model_epoch_{best_epoch}_seed_{seed}.pth")
    if args.neptune:
        run.stop()


if __name__ == "__main__":
    main()
