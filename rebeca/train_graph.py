"""Module for training neural network models."""
import os
import pprint
from pathlib import Path

import neptune.new as neptune
import numpy as np
import pandas as pd
import torch
import torch.nn as nn
import torch.optim as optim
import torch_geometric as tg
from torch_geometric.utils import degree
from torch.utils.data import DataLoader
from torch.utils.data.sampler import SequentialSampler
from torch.optim.lr_scheduler import ReduceLROnPlateau
from tqdm import tqdm

import rebeca.data.dataset as da
import rebeca.model.sequence_model as sm
from rebeca.core import load_cfg, parse_args
from rebeca.data.dataset import GroupedBatchSampler, UTRDataset
from rebeca.util.loss_function import kl_divergence

# from rebeca.model.sequence_model import DoubleConv, init_weights,CONVBILSTM,REC,MLP
from rebeca.util.training_utils import evaluate_graph, save_checkpoint, train_batch, val_batch

# Load args
args = parse_args()
# Load config.yml
cfg = load_cfg(args.config / "config.yml")

# Print selected configuration
pp = pprint.PrettyPrinter(indent=4)
pp.pprint(cfg)

def main():  # noqa: CCR001
    """Main script."""
    # Selecting hardware
    device = torch.device(cfg['GPU'] if torch.cuda.is_available() else "cpu")
    print(f"Using {device} device")

    # Reproducibility settings
    seed = 32 #3 for all experiments, 11 and 32 for rep.
    np.random.seed(seed)
    torch.manual_seed(seed)
    torch.backends.cudnn.deterministic = True
    torch.backends.cudnn.benchmark = False  # switch on optimiser for cuDNN algorithms for computing convolutions, switch off for reproducibility

    # Load args
    args = parse_args()

    # Experiment Tracking
    if args.neptune:
        run = neptune.init_run(
            project="pag/rebeca",
            api_token=os.environ["NEPTUNE_API_TOKEN"],
            capture_stdout=False,
            capture_stderr=False,
        )
        with open(Path(args.config) / "neptune_run.txt", mode="w") as f:
            f.write(f"The url for the neptune run is {run.get_url()}")

    # Load data
    path_raw_data=Path(cfg["param_dataset"]['path_raw_data'])
    path_root_graph = Path(cfg["param_dataset"]['path_root_graph'])

    # import time
    # pin_memory = True
    # print('pin_memory is', pin_memory)

    # for num_workers in range(0, 20, 1): 
    #     train_dataloader = tg.loader.DataLoader(
    #     getattr(da, cfg["dataset"])(root=path_root_graph / 'train',path_raw_data=path_raw_data / 'train/raw_dir/',
    #     graph_type=cfg["param_dataset"]['graph_type'],df_path=cfg["param_dataset"]['df_path'],num_samples=cfg["param_dataset"]['num_samples_train'],
    #     output_type=cfg["param_dataset"]['output_type']),
    #     batch_size=cfg["train"]["batch_size"],
    #     shuffle=True,pin_memory=pin_memory,num_workers=num_workers)
    #     start = time.time()
    #     for epoch in range(1, 5):
    #         for i, data in enumerate(train_loader):
    #             pass
    #     end = time.time()
    #     print("Finish with:{} second, num_workers={}".format(end - start, num_workers))

    print("ddyp", cfg["param_dataset"]["path_raw_data"])

    train_dataloader = tg.loader.DataLoader(
        getattr(da, cfg["dataset"])(root=path_root_graph / 'train',path_raw_data=path_raw_data / 'train/',
        graph_type=cfg["param_dataset"]['graph_type'],df_path=cfg["param_dataset"]['df_path'],num_samples=cfg["param_dataset"]['num_samples_train'],
        output_type=cfg["param_dataset"]['output_type'],length_sequence=cfg["param_dataset"]['length_sequence']),
        batch_size=cfg["train"]["batch_size"],
        num_workers=cfg["train"]["num_workers"],
        shuffle=True,
    )
    val_dataloader = tg.loader.DataLoader(
        getattr(da, cfg["dataset"])(root=path_root_graph / 'val',path_raw_data=path_raw_data / 'val/',
        graph_type=cfg["param_dataset"]['graph_type'],df_path=cfg["param_dataset"]['df_path'],num_samples=cfg["param_dataset"]['num_samples_val'],
        output_type=cfg["param_dataset"]['output_type'],length_sequence=cfg["param_dataset"]['length_sequence']),
        batch_size=cfg["train"]["batch_size"],
        num_workers=cfg["train"]["num_workers"],
        shuffle=True,
    )


    if cfg["architecture"]["name"]=='BasePNA':
        # Compute the maximum in-degree in the training data.
        max_degree = -1
        for data in train_dataloader:
            d = degree(data.edge_index[1], num_nodes=data.num_nodes, dtype=torch.long)
            max_degree = max(max_degree, int(d.max()))

        # Compute the in-degree histogram tensor
        deg = torch.zeros(max_degree + 1, dtype=torch.long)
        for data in train_dataloader:
            d = degree(data.edge_index[1], num_nodes=data.num_nodes, dtype=torch.long)
            deg += torch.bincount(d, minlength=deg.numel())
        cfg["param_architecture"]["deg"]=deg
        

    # Load model
    model = getattr(sm, cfg["architecture"]["name"])(**cfg["param_architecture"])
    model.apply(sm.init_weights)
    print("the model is", model)
        # Specifying hardware
    if torch.cuda.is_available():
        model.to(device)  # you need to send your model weights to the GPU

    else:
        num_workers = 0
        batch_size = cfg["train"]["batch_size"]
    model_parameters = filter(lambda p: p.requires_grad, model.parameters())
    params = sum([np.prod(p.size()) for p in model_parameters])
    print('the number of param is ',params)

    # Declare loss function and optimizer
    try: 
        loss_fn = getattr(nn, cfg["loss"]["name"])()
    except AttributeError:
        loss_fn=kl_divergence
    optimizer = getattr(optim, cfg["optimizer"]["name"])(
        model.parameters(), lr=float(cfg["optimizer"]["lr"])
    )
    scheduler = ReduceLROnPlateau(optimizer, mode='min', factor=0.5, patience=3,
                              min_lr=1e-5)
    scaler=torch.cuda.amp.GradScaler()


    best_loss = 100
    best_epoch = 0
    num_epochs, warmup_epochs, patience = (
        int(cfg["train"]["num_epochs"]),
        int(cfg["train"]["warmup_epochs"]),
        int(cfg["train"]["patience"]),
    )
    for epoch in range(num_epochs):  # loop over the dataset multiple times
        model.train()
        loss = []
        loss_val = []
        loop = tqdm(train_dataloader, total=int(len(train_dataloader)))
        # for data in list(train_dataloader)[:10]:
        for _, data in enumerate(loop):
            data = data.to(device)
            loss_batch = train_batch(model, data, data.y, loss_fn, optimizer,scaler)
            loss.append(loss_batch)
            if args.neptune:
                # log on neptune
                run["train/batch/loss"].log(loss_batch)
        if args.neptune:
            # log on neptune
            run["train/batch/epoch_loss"].log(np.mean(loss))
        # update progress bar
        loop.set_postfix(loss=np.mean(loss))
        loop.set_description(f"Epoch [{epoch}/{num_epochs}]")

        with torch.no_grad():
            model.eval()
            for data in val_dataloader:
                data = data.to(device)
                loss_val_batch = val_batch(model, data, data.y, loss_fn)
                loss_val.append(loss_val_batch)
                if args.neptune:
                    run["val/batch/loss"].log(loss_val_batch)
            val_loss_epoch = np.mean(loss_val)

            if args.neptune:
                run["val/distribution"].log(
                    evaluate_graph(val_dataloader, model, epoch, "validation", device)
                )
                run["val/batch/epoch_loss"].log(np.mean(loss_val))
            if val_loss_epoch < best_loss-0.0012 and epoch > warmup_epochs:
                best_loss = val_loss_epoch
                best_epoch = epoch
                # keep snapshot
                best_model = model.state_dict()
        if epoch - best_epoch > patience:
            print("training finished")
            if args.neptune:
                # Log PyTorch model weights
                run["model_checkpoints/my_model"].upload(
                    f"model_checkpoints/best_model_epoch_{epoch}.pth"
                )
            break
    # At this point save a snapshot of the best model
    torch.save(best_model, Path(args.config) / f"best_model_epoch_{best_epoch}.pth")
    if args.neptune:
        run.stop()


if __name__ == "__main__":
    main()
