"""Create Dataset class."""
import math
import os
from collections import defaultdict
from itertools import chain, repeat
from pathlib import Path
from typing import Dict, Iterator, List, Optional, Tuple, Union

import numpy as np
import pandas as pd
import itertools
import torch
import torch.nn as nn
import torch_geometric as tg
from tqdm import tqdm
from torch import Tensor
from torch.utils.data import Dataset
from torch.utils.data.sampler import BatchSampler, Sampler


def encode_padding(seq):
    """One_hot encode RNA sequence.

    Args:
        seq: mRNA 5'UTR sequence. Only AUCGZ letters are accepted, Z being the letter used for padding.

    Returns:
        one hot encoding of padded mRNA 5'UTR sequence.
    """
    mapping = dict(zip("ACGUZ", range(5)))
    seq=seq.upper()
    seq = seq.replace('T', 'U')
    seq2 = [mapping[i] for i in seq]
    mat = np.zeros((5, 5))
    mat[:4, :4] = np.eye(4)
    return mat[seq2].T[:4, :]


def encode(seq):
    """One_hot encode RNA sequence.

    Args:
        seq: mRNA 5'UTR sequence. Only AUCG letters are accepted.

    Returns:
        one hot encoding of mRNA 5'UTR sequence.(4*UTR_length) matrix
    """
    mapping = dict(zip("ACGU", range(4)))
    seq = seq.replace('T', 'U')
    seq2 = [mapping[i] for i in seq]
    return np.eye(4)[seq2].T


class UTRDataset(Dataset):
    """Class to create 4*30 images from utr sequences with
    mode bin as  target output."""

    def __init__(self, path_data, split, journal, utr_max_length, padding=True):
        """Initialise class."""
        # self.utr_df = pd.read_csv(Path("rebeca/data") / f"{journal}_{split}.csv")
        self.utr_df = pd.read_csv(Path(path_data) / f"{journal}_{split}.csv")
        self.utr_max_length = utr_max_length
        self.padding = padding

    def __len__(self):
        """Length of the dataset."""
        return len(self.utr_df)

    def __getitem__(self, idx):
        """Returning an item."""
        sequence_idx = self.utr_df.iloc[idx, 0]
        if self.padding:
            image = torch.from_numpy(encode_padding(sequence_idx)).double()
        else:
            image = torch.from_numpy(encode(sequence_idx)).double()
        # pad if necessary
        # m = nn.ConstantPad1d((self.utr_max_length - len(self.utr_df.iloc[idx, 0]), 0), 0)
        # image = m(image)
        label = torch.tensor(self.utr_df.iloc[idx, 1] - 1)
        return image, label, sequence_idx


class UTRDistributionDataset(Dataset):
    """Class to create 4*30 images from utr sequences
    with distribution as target output."""

    def __init__(self, path_data, split, journal, utr_max_length, padding=True):
        """Initialise class."""
        # self.utr_df = pd.read_csv(Path("rebeca/data") / f"{journal}_{split}.csv")
        self.utr_df = pd.read_csv(Path(path_data) / f"{journal}_{split}.csv")
        self.utr_max_length = utr_max_length
        self.padding = padding

    def __len__(self):
        """Length of the dataset."""
        return len(self.utr_df)

    def __getitem__(self, idx):
        """Returning an item."""
        sequence_idx = self.utr_df.iloc[idx, 0]
        if self.padding:
            image = torch.from_numpy(encode_padding(sequence_idx)).double()
        else:
            image = torch.from_numpy(encode(sequence_idx)).double()
        # pad if necessary
        # m = nn.ConstantPad1d((self.utr_max_length - len(self.utr_df.iloc[idx, 0]), 0), 0)
        # image = m(image)
        label = torch.tensor(list(self.utr_df.iloc[idx, 1:]))
        return image, label, sequence_idx

class UTRGaussianDataset(Dataset):
    """Class to create 4*30 images from utr sequences
    with gaussian as output."""

    def __init__(self, path_data, split, journal, utr_max_length, padding=True):
        """Initialise class."""
        # self.utr_df = pd.read_csv(Path("rebeca/data") / f"{journal}_{split}.csv")
        self.utr_df = pd.read_csv(Path(path_data) / f"{journal}_{split}.csv")
        self.utr_max_length = utr_max_length
        self.padding = padding

    def __len__(self):
        """Length of the dataset."""
        return len(self.utr_df)

    def __getitem__(self, idx):
        """Returning an item."""
        sequence_idx = self.utr_df.iloc[idx, 0]
        if self.padding:
            image = torch.from_numpy(encode_padding(sequence_idx)).double()
        else:
            image = torch.from_numpy(encode(sequence_idx)).double()
        gaussian_parameters = torch.tensor(list(self.utr_df.iloc[idx, -2:]))
        return image, gaussian_parameters, sequence_idx

class UTRGaussianRandomSDDataset(Dataset):
    """Class to create 4*30 images from utr sequences
    with gaussian parameters as output. The sd has been shuffled"""

    def __init__(self, path_data, split, journal, utr_max_length, padding=True):
        """Initialise class."""
        # self.utr_df = pd.read_csv(Path("rebeca/data") / f"{journal}_{split}.csv")
        self.utr_df = pd.read_csv(Path(path_data) / f"{journal}_{split}.csv")
        self.utr_df['sigma_mle']=self.utr_df['sigma_mle'].sample(frac=1).to_numpy()
        self.utr_max_length = utr_max_length
        self.padding = padding

    def __len__(self):
        """Length of the dataset."""
        return len(self.utr_df)

    def __getitem__(self, idx):
        """Returning an item."""
        sequence_idx = self.utr_df.iloc[idx, 0]
        if self.padding:
            image = torch.from_numpy(encode_padding(sequence_idx)).double()
        else:
            image = torch.from_numpy(encode(sequence_idx)).double()
        gaussian_parameters = torch.tensor(list(self.utr_df.iloc[idx, -2:]))
        return image, gaussian_parameters, sequence_idx


class UTRMeanFluoDataset(Dataset):
    """Class to create 4*30 images from utr sequences
    with log mean fluo as output."""

    def __init__(self, path_data, split, journal, utr_max_length, padding=True):
        """Initialise class."""
        # self.utr_df = pd.read_csv(Path("rebeca/data") / f"{journal}_{split}.csv")
        self.utr_df = pd.read_csv(Path(path_data) / f"{journal}_{split}.csv")
        self.utr_max_length = utr_max_length
        self.padding = padding

    def __len__(self):
        """Length of the dataset."""
        return len(self.utr_df)

    def __getitem__(self, idx):
        """Returning an item."""
        sequence_idx = self.utr_df.iloc[idx, 0]
        sequence_idx=sequence_idx[-self.utr_max_length:]
        if self.padding:
            sequence_idx+='Z'*(self.utr_max_length-len(sequence_idx))
            image = torch.from_numpy(encode_padding(sequence_idx)).half()
            # print(sequence_idx,image)
        else:
            image = torch.from_numpy(encode(sequence_idx)).half()
        gaussian_parameters = torch.tensor(list(self.utr_df.iloc[idx, -1:])).half()
        # print(image, gaussian_parameters, sequence_idx)
        return image, gaussian_parameters, sequence_idx

class UTREmbeddingMeanFluoDataset(Dataset):
    """Class to create 4*620 embeddings from utr sequences
    with log mean fluo as output."""

    def __init__(self, path_data, split, journal, utr_max_length, padding=True):
        """Initialise class."""
        # self.utr_df = pd.read_csv(Path("rebeca/data") / f"{journal}_{split}.csv")
        self.utr_df = pd.read_csv(Path(path_data) / f"{journal}_{split}.csv")
        self.utr_max_length = utr_max_length
        self.path=Path(path_data)/ f"{journal}_{split}/"
        self.padding = padding

    def __len__(self):
        """Length of the dataset."""
        return len(self.utr_df)

    def __getitem__(self, idx):
        """Returning an item."""
        sequence_idx = self.utr_df.iloc[idx, 0]
        embedding_array=np.load(self.path / f'{int(idx+1)}.npy')[:30,:].T
        if self.padding:
            image = torch.from_numpy(embedding_array).half()
        else:
            image = torch.from_numpy(embedding_array).half()
        gaussian_parameters =  torch.tensor([self.utr_df.iloc[idx, 1]]).half() 
        return image, gaussian_parameters, sequence_idx

class UTRMeanFluoPromoterDataset(Dataset):
    """Class to create 4*30 images from utr sequences
    with log mean fluo as output."""

    def __init__(self, path_data, split, journal, utr_max_length,promoter='all', padding=True):
        """Initialise class."""
        # self.utr_df = pd.read_csv(Path("rebeca/data") / f"{journal}_{split}.csv")
        self.utr_df = pd.read_csv(Path(path_data) / f"{journal}_{split}.csv")
        if promoter!='all':
            self.utr_df= self.utr_df[self.utr_df['promoter']==promoter]
        self.utr_max_length = utr_max_length
        self.padding = padding

    def __len__(self):
        """Length of the dataset."""
        return len(self.utr_df)

    def __getitem__(self, idx):
        """Returning an item."""
        sequence_idx = self.utr_df.iloc[idx, 0]
        sequence_idx=sequence_idx[:self.utr_max_length]
        sequence_idx+='Z'*(self.utr_max_length-len(sequence_idx))
        if self.padding:
            image = torch.from_numpy(encode_padding(sequence_idx)).half()
        else:
            image = torch.from_numpy(encode(sequence_idx)).half()
        gaussian_parameters = torch.tensor([self.utr_df.iloc[idx, 1]]).half()
        return image, gaussian_parameters, sequence_idx

def _repeat_to_at_least(iterable, n):
    repeat_times = math.ceil(n / len(iterable))
    repeated = chain.from_iterable(repeat(iterable, repeat_times))
    return list(repeated)


class BucketizeBatchSampler(BatchSampler):
    """Buketized BatchSampler for sequential data with different lengths to reduce number of paddings.
    Args:
        lengths (List[int]): The lengths of the samples in the dataset.
        num_buckets (int): The number of buckets to split the data samples.
        min_len (int, optional): The minimum sample lengths to keep.
            (Default: 0)
        max_len (int or None, optional): The maximum sample lengths to keep. Inferred if not provided.
            (Default ``None``)
        max_token_count (int or None, optional): The max number of tokens in one mini-batch.
            (Default: ``None``)
        batch_size (int or None, optional): The number of samples in one mini-batch.
            (Default: ``None``)
        shuffle (bool, optional): Whether to shuffle buckets for non-monotonic length sampling.
            (Default: True)
        drop_last (bool, optional): If ``True``, the sampler will drop the last batch if
            its size would be less than ``batch_size``
            (Default: False)
    Note:
        ``max_token_count`` and ``batch_size`` are mutually exclusive. Only one argument of the two
        should have value.
    Note:
        ``drop_last`` is only valid when ``batch_size`` argument is given.
    """

    def __init__(
        self,
        lengths: List[int],
        num_buckets: int,
        min_len: int = 0,
        max_len: Optional[int] = None,
        max_token_count: Optional[int] = None,
        batch_size: Optional[int] = None,
        shuffle: bool = True,
        drop_last: bool = False,
    ) -> None:
        if max_len is None:
            max_len = max(lengths)

        if not (0 <= min_len <= max_len):
            raise AssertionError("``min_len`` should be non-negative and smaller than ``max_len``")
        if max_token_count is not None and batch_size is not None:
            raise AssertionError("The ``max_token_count`` and ``batch_size`` can't be both set.")
        if max_token_count is None and batch_size is None:
            raise AssertionError("One of ``max_token_count`` or ``batch_size`` must be set.")
        if max_token_count is not None:
            assert (
                max_len <= max_token_count
            ), "The  ``max_token_count`` must be greater than or equal to the maximum value of ``lengths``."
        # Filter out samples which are outside the bounds of [min_len, max_len]
        filtered_length_idx = [
            (length, i) for i, length in enumerate(lengths) if min_len <= length <= max_len
        ]
        if len(filtered_length_idx) == 0:
            raise AssertionError("``lengths`` cannot be empty after filtering.")
        sorted_filtered_length_idx = sorted(filtered_length_idx, key=lambda x: x[0])
        self.lengths = [e[0] for e in sorted_filtered_length_idx]
        self.indices = [e[1] for e in sorted_filtered_length_idx]
        self.max_token_count = max_token_count
        self.batch_size = batch_size
        self.shuffle = shuffle
        self.drop_last = drop_last
        self.buckets = self._get_buckets(self.lengths, num_buckets, min_len, max_len)
        self.iter_list = []
        self._update_iter_list()

    def _get_buckets(
        self, lengths: List[int], num_buckets: int, min_len: int, max_len: int
    ) -> Dict[int, Tensor]:
        """Generate buckets based on the dataset.
        Args:
            lengths (List[int]): The lengths of the samples in the dataset.
            num_buckets (int): The number of buckets.
            min_len (int): The lower bound of the evenly spaced length intervals to determine bucket width.
            max_len (int): The upper bound of the evenly spaced length intervals to determine bucket width.
        Returns:
            (dict[int, Tensor]): A dictionary in which the key is the bucket index, the value is
                the Tensor of corresponding sample indices.
        """
        buckets = {}
        boundaries = torch.linspace(min_len - 1, max_len + 1, num_buckets + 1)
        bucket_ids = torch.bucketize(torch.tensor(lengths), boundaries)
        for i in range(bucket_ids.size(0)):
            bucket_id = int(bucket_ids[i])
            if bucket_id in buckets:
                buckets[bucket_id].append(i)
            else:
                buckets[bucket_id] = [i]
        for k in buckets:
            buckets[k] = torch.as_tensor(buckets[k], dtype=torch.int)
        buckets = {k: v for k, v in sorted(buckets.items())}
        return buckets

    def _update_iter_list(self) -> None:
        self.iter_list = []
        total_len = 0
        batch = []
        max_batch_size = self.max_token_count if self.max_token_count else self.batch_size
        for k in self.buckets:
            for i in range(self.buckets[k].size(0)):
                index = int(self.buckets[k][i])
                sample_length = self.lengths[index] if self.max_token_count else 1
                if total_len + sample_length <= max_batch_size:
                    batch.append(self.indices[index])
                    total_len += sample_length
                else:
                    self.iter_list.append(batch)
                    batch = [self.indices[index]]
                    total_len = sample_length
        if len(batch) > 0 and (self.max_token_count or not self.drop_last):
            self.iter_list.append(batch)

    def __iter__(self) -> Iterator[List[int]]:
        if self.shuffle:
            for k in self.buckets:
                self.buckets[k] = self.buckets[k][torch.randperm(self.buckets[k].size(0))]
            self._update_iter_list()

        return iter(self.iter_list)

    def __len__(self):
        if self.batch_size or (self.max_token_count and not self.shuffle):
            return len(self.iter_list)


class GroupedBatchSampler(BatchSampler):
    """
    Wraps another sampler to yield a mini-batch of indices.
    It enforces that the batch only contain elements from the same group.
    It also tries to provide mini-batches which follows an ordering which is
    as close as possible to the ordering from the original sampler.
    Args:
        sampler (Sampler): Base sampler.
        group_ids (list[int]): If the sampler produces indices in range [0, N),
            `group_ids` must be a list of `N` ints which contains the group id of each sample.
            The group ids must be a continuous set of integers starting from
            0, i.e. they must be in the range [0, num_groups).
        batch_size (int): Size of mini-batch.
    """

    def __init__(self, sampler, group_ids, batch_size):
        if not isinstance(sampler, Sampler):
            raise ValueError(
                f"sampler should be an instance of torch.utils.data.Sampler, but got sampler={sampler}"
            )
        self.sampler = sampler
        self.group_ids = group_ids
        self.batch_size = batch_size

    def __iter__(self):
        buffer_per_group = defaultdict(list)
        samples_per_group = defaultdict(list)

        num_batches = 0
        for idx in self.sampler:
            group_id = self.group_ids[idx]
            buffer_per_group[group_id].append(idx)
            samples_per_group[group_id].append(idx)
            if len(buffer_per_group[group_id]) == self.batch_size:
                yield buffer_per_group[group_id]
                num_batches += 1
                del buffer_per_group[group_id]
            assert len(buffer_per_group[group_id]) < self.batch_size

        # now we have run out of elements that satisfy
        # the group criteria, let's return the remaining
        # elements so that the size of the sampler is
        # deterministic
        expected_num_batches = len(self)
        num_remaining = expected_num_batches - num_batches
        if num_remaining > 0:
            # for the remaining batches, take first the buffers with largest number
            # of elements
            for group_id, _ in sorted(
                buffer_per_group.items(), key=lambda x: len(x[1]), reverse=True
            ):
                remaining = self.batch_size - len(buffer_per_group[group_id])
                samples_from_group_id = _repeat_to_at_least(samples_per_group[group_id], remaining)
                buffer_per_group[group_id].extend(samples_from_group_id[:remaining])
                assert len(buffer_per_group[group_id]) == self.batch_size
                yield buffer_per_group[group_id]
                num_remaining -= 1
                if num_remaining == 0:
                    break
        assert num_remaining == 0

    def __len__(self):
        return len(self.sampler) // self.batch_size


class RNA2D(tg.data.Dataset):
    def __init__(
        self,
        root,
        path_raw_data,
        df_path,
        graph_type,
        num_samples,
        output_type,
        length_sequence,
        pos_emb=False,
        transform=None,
        pre_transform=None,
        **kwargs,
    ):
        self.path_raw_data = Path(path_raw_data)
        self.graph_type = graph_type
        self.df_label = pd.read_csv(df_path) # dataframe with sequence and target output
        self.df_label['UTR_CDS']=self.df_label.iloc[:,0].str.upper()
        self.df_label['UTR_CDS']=self.df_label['UTR_CDS'].str.replace("T",'U')
        self.num_samples = num_samples
        self.output_type=output_type
        self.pos_emb=pos_emb
        self.length_sequence=length_sequence
        Path(root).mkdir(parents=True, exist_ok=True)
        super().__init__(root, transform, pre_transform, **kwargs)

    @property
    def processed_file_names(self) -> List[str]:
        """A list of files in the processed_dir which needs to be found in order to skip the processing.

        Returns:
            list of file names for processed files
        """
        return [f"data_{i}.pt" for i in range(self.num_samples)]

    def process(self):
        idx = 0
        if self.graph_type in ["v3","v4","v5","v3.1"]:
            list_examples=list(self.path_raw_data.glob("*.txt"))
        else:
            list_examples=list(self.path_raw_data.glob("*.ct"))
        loop=tqdm(list_examples,total=int(len(list_examples)))
        for _, path in enumerate(loop):
            # Read data from `raw_path`.
            data = build_graph(path, self.graph_type, self.df_label,self.output_type,self.pos_emb,self.length_sequence)
            torch.save(data, os.path.join(self.processed_dir, f"data_{idx}.pt"))
            idx += 1

    def len(self):
        return self.num_samples

    def get(self, idx):
        data = torch.load(os.path.join(self.processed_dir, f"data_{idx}.pt"))
        return data


def build_graph(file_path, graph_type, df_label,output_type,pos_emb,length_sequence):
    # Base connectivity: adjacent nucleic acids are connected
    edge_index = [
        list(range(length_sequence-1)) + list(range(length_sequence-1, 0, -1)),
        list(range(1, length_sequence)) + list(range(length_sequence-2, -1, -1)),
    ]
    if graph_type=='v4':
        #Only base pairing connectivity matter, not adjacent nucleotides (O(nlogn )-O(n))
        edge_attr=[]
        edge_index=[[],[]]
        with open(file_path, "r") as f:
            sequence = ""
            idx=0
            for line in f:
                info=line.split()
                sequence+=info[1] # get sequence
                for pairing in info[2:]:
                    next_nucleotide,probability=pairing.split(":")
                    next_nucleotide,probability=int(next_nucleotide)-1,100*float(probability)
                    edge_index[0]+=[idx,next_nucleotide]
                    edge_index[1]+=[next_nucleotide,idx]
                    edge_attr+=[[probability],[probability]]
                idx+=1
    if graph_type=='v5':
        #Only base pairing connectivity matter, and we look at the log prob #O(nlogn)-O(n)
        edge_attr=[]
        edge_index=[[],[]]
        with open(file_path, "r") as f:
            sequence = ""
            idx=0
            for line in f:
                info=line.split()
                sequence+=info[1] # get sequence
                for pairing in info[2:]:
                    next_nucleotide,probability=pairing.split(":")
                    next_nucleotide,probability=int(next_nucleotide)-1,np.log(float(probability))
                    edge_index[0]+=[idx,next_nucleotide]
                    edge_index[1]+=[next_nucleotide,idx]
                    edge_attr+=[[probability],[probability]]
                idx+=1
    elif graph_type=='v3': #O(nlogn)-O(n)
        edge_attr=[[1,0] for _ in range(len(edge_index[0]))]
        with open(file_path, "r") as f:
            sequence = ""
            idx=0
            for line in f:
                info=line.split()
                sequence+=info[1] # get sequence
                for pairing in info[2:]:
                    next_nucleotide,probability=pairing.split(":")
                    next_nucleotide,probability=int(next_nucleotide)-1,100*float(probability)
                    edge_index[0]+=[idx,next_nucleotide]
                    edge_index[1]+=[next_nucleotide,idx]
                    edge_attr+=[[0,probability],[0,probability]]
                idx+=1

    elif graph_type=='v3.1': #O(nlogn)-O(n)
        edge_attr=[[1,0] for _ in range(len(edge_index[0]))]
        with open(file_path, "r") as f:
            sequence = ""
            idx=0
            for line in f:
                info=line.split()
                sequence+=info[1] # get sequence
                for pairing in info[2:]:
                    next_nucleotide,probability=pairing.split(":")
                    next_nucleotide,probability=int(next_nucleotide)-1,1
                    edge_index[0]+=[idx,next_nucleotide]
                    edge_index[1]+=[next_nucleotide,idx]
                    edge_attr+=[[0,probability],[0,probability]]
                idx+=1
    else:
        with open(file_path, "r") as f:
            id_construct = f.readline() #keep if header on ct files has a header
            sequence = ""
            for line in f:
                # print(sequence)
                a, b = [int(x) for x in line.split()[-2:]]
                sequence += line.split()[1]
                if a == 0:
                    continue
                if graph_type == "v1" or graph_type=="v2": #secondary structure informed connectivity matrix 
                    edge_index[0].append(a - 1)
                    edge_index[1].append(b - 1)
                # if graph_type == "v2": #fully connected graph with weights equal to 1 if base pairing (default pytorch behaviour),2 if adjacent and 0 otherwise
                #     edge_weight
            if graph_type=="v2":
                edge_attr=[[1,0,0] for _ in range(2*length_sequence-2)]+[[0,1,0] for _ in range(len(edge_index[0])-2*length_sequence)] #check zhy 118
                #get remaining edges
                all_edges=set(itertools.product(list(range(length_sequence)),repeat=2))-set([(i,i) for i in range(length_sequence)])
                to_add=all_edges-set(zip(*edge_index))
                to_add=list(zip(*to_add))
                edge_index[0]+=to_add[0]
                edge_index[1]+=to_add[1]
                edge_attr+=[[0,0,1] for _ in range(len(to_add[0]))]
    # get label
    sequence=sequence.upper().replace("T",'U')
    if output_type=='bins':
        label = df_label.loc[df_label["UTR_CDS"] == f"{sequence}"].iloc[0, 1:].values.tolist()
    if output_type=='gaussian':
        label = list(df_label.loc[df_label["UTR_CDS"] == f"{sequence}"].iloc[0, -2:])
    if output_type=='mean_fluorescence':
        if len(df_label.loc[df_label["UTR_CDS"] == f"{sequence}"])==0:
            print(sequence,df_label[df_label["UTR_CDS"] == f"{sequence}"])
        else:
            label = df_label.loc[df_label["UTR_CDS"] == f"{sequence}"].iloc[0, 1].tolist()

    if graph_type=="v2" or graph_type=='v3' or graph_type=='v4' or graph_type=='v5':
        data = tg.data.Data(
            x=torch.from_numpy(encode(sequence).T).float(),
            edge_index=torch.Tensor(edge_index).long(),
            edge_attr=torch.Tensor(edge_attr).float(),
            y=torch.Tensor([label]).float(),
            sequence=sequence,
        )
    else:
        data = tg.data.Data(
            x=torch.from_numpy(encode(sequence).T).float(),
            edge_index=torch.Tensor(edge_index).long(),
            y=torch.Tensor([label]).float(),
            sequence=sequence,
        )
    if pos_emb:
        pe=[[x/(1000**(k/2)) for x in range(1,len(seq)+1)] for k in range(1,5)]
        total_pe=np.vstack((np.sin(pe[0]),np.cos(pe[1]),np.sin(pe[2]),np.cos(pe[3])))
        data.x+=torch.from_numpy(pe).float()

    return data

