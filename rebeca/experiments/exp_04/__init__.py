"""EXP_04
GOAL: test influence of thE training set sample size on the model performance
METHOD: datasets ranging from 22,000 to 1,000 (roughly) training examples. No data augmentation was performed.
"""
