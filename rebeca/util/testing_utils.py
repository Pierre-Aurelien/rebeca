"""Functions to declutter test script."""
from pathlib import Path
from random import choices, seed
from typing import Dict, List, Tuple

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
import torch
from tqdm import tqdm
from sklearn.metrics import precision_recall_fscore_support
import torch_geometric as tg

def get_accuracy(y_true: [List[float]], y_pred: [List[float]]) -> float:
    """
    Compute accuracy
    Args:
        y_true: ground truth
        y_pred: predictions

    Returns:
    accuracy metric
    """
    accuracy = int(100 * ([x - y for (x, y) in zip(y_true, y_pred)].count(0) / len(y_true)))
    return accuracy


def get_ext_accuracy(y_true: [List[float]], y_pred: [List[float]]) -> float:
    """
    Compute extended accuracy: Predictions + or - 1 bin from the truth are considered good.
    Args:
        y_true: ground truth
        y_pred: predictions

    Returns:
    accuracy metric
    """
    extended_accuracy = 100 * (
        [np.abs(x - y) for (x, y) in zip(y_true, y_pred)].count(1) / len(y_true)
    ) + get_accuracy(y_true, y_pred)
    return extended_accuracy


def bootstrap_estimate_and_ci(
    target_type,
    y_true: [List[float]],
    y_pred: [List[float]],
    loss_value,
    alpha: float = 0.05,
    bs_samples: int = 1000,
):
    """Computes estimate and ci from bootstrap scores"""
    if target_type=='gaussian':
        scores = get_bs_mape(y_true, y_pred, bs_samples)
    else:
        scores = get_bs_samples(y_true, y_pred, bs_samples)  # metrics

    scores = np.column_stack((scores, get_bs_loss(loss_value, bs_samples)))  # add loss
    estimate = np.mean(scores, axis=0)
    lower_bound = np.percentile(scores, 100 * (alpha / 2), axis=0)
    upper_bound = np.percentile(scores, 100 * (1 - alpha / 2), axis=0)
    stderr = np.std(scores, axis=0)
    return np.array([estimate, lower_bound, upper_bound, stderr]), scores


def get_bs_samples(y_true: [List[float]], y_pred: [List[float]], bs_samples: int = 1000):
    # get bootstrap samples of macro metrics.
    to_permute = list(zip(y_true, y_pred))
    scores = np.empty((bs_samples, 2))
    for _, rep in enumerate(range(bs_samples)):
        seed(rep)
        permuted_list = list(zip(*choices(to_permute, k=len(to_permute))))
        scores[rep, :] = get_macro_metrics(permuted_list[0], permuted_list[1])
    return scores

def get_bs_loss(loss_value: [List[float]], bs_samples: int = 1000):
    # get bootstrap samples of macro metrics.
    scores = np.empty((bs_samples, 1))
    for _, rep in enumerate(range(bs_samples)):
        seed(rep)
        permuted_list = choices(loss_value, k=len(loss_value))
        scores[rep, 0] = np.mean(permuted_list)
    return scores

def get_bs_mape(y_true: [List[float]], y_pred: [List[float]], bs_samples: int = 1000):
    """
    y_true: list of the target output for each input element
    y_pred: list of the predicted target output for each input element
    """
    # get bootstrap samples of macro metrics.
    to_permute = list(zip(y_true, y_pred))
    scores = np.empty((bs_samples, 2))
    for _, rep in enumerate(range(bs_samples)):
        seed(rep)
        permuted_list = list(zip(*choices(to_permute, k=len(to_permute))))
        scores[rep, :] = get_mape(permuted_list[0], permuted_list[1])
    return scores

def get_mape(y_true: [List[float]], y_pred: [List[float]]):
    if len(y_true[0])==2:
        ape_mu=[np.abs(y_true[i][0]-y_pred[i][0])/y_true[i][0] for i in range(len(y_true))]
        mape_mu=np.mean(ape_mu)
        ape_sigma=[np.abs(y_true[i][1]-np.exp(y_pred[i][1]))/y_true[i][1] for i in range(len(y_true))]
        mape_sigma=np.mean(ape_sigma)
        return [mape_mu,mape_sigma]
    else:
        ape_mu=[np.abs(y_true[i][0]-y_pred[i][0])/(y_true[i][0]+1e-16) for i in range(len(y_true))]
        mape_mu=np.mean(ape_mu)
        return [mape_mu]       


def get_macro_metrics(y_true: [List[float]], y_pred: [List[float]]):
    accuracy = get_accuracy(y_true, y_pred)
    extended_accuracy = get_ext_accuracy(y_true, y_pred)
    classes = ["0", "1", "2", "3", "4", "5", "6", "7"]
    y_true = [f"{x}" for x in y_true]
    y_pred = [f"{x}" for x in y_pred]
    metrics = list(precision_recall_fscore_support(y_true, y_pred, average="macro"))[:-1]
    return [accuracy, extended_accuracy] + metrics


def get_output(data_loader, model, loss_fn,target_type):
    """Returns mode bin (predicted and ground truth) with the loss."""
    y_p = []  # predicted mode bin
    y_t = []  # true mode bin
    x_s = []  # sequence
    y_l = []  # Cross-entropy loss
    with torch.no_grad():
        for data in list(data_loader):
            if isinstance(data,tg.data.Data):
                inputs, labels, sequence = data,data.y,data.sequence
            else:
                inputs, labels, sequence = data
            # inputs, labels = inputs.to(device), labels.to(device)  # Send input tensors to the GPU
            print('hey',inputs, model(inputs) )
            # break
            logits = model(inputs)  # (batch, num_classes)
            loss = loss_fn(logits, labels)
            if labels.size() == logits.size():
                mode_bin = labels.argmax(dim=1)
                predicted_mode_bin = logits.argmax(dim=1)
            if target_type == 'gaussian':
                mode_bin=labels
                predicted_mode_bin=logits
            else:
                mode_bin = labels
                predicted_mode_bin = logits.argmax(dim=1)
            y_t += mode_bin.tolist()
            y_p += predicted_mode_bin.tolist()
            x_s += list(sequence)
            y_l += [float(loss)]
    return y_t, y_p, y_l, x_s



def get_output_graph(data_loader, model, loss_fn):
    """Returns mode bin (predicted and ground truth) with the loss."""
    y_p = []  # predicted mode bin
    y_t = []  # true mode bin
    x_s = []  # sequence
    y_l = []  # Cross-entropy loss
    with torch.no_grad():
        model.eval()
        loop = tqdm(data_loader, total=int(len(data_loader)))
        for _, data in enumerate(loop):
            softmax = model(data)  # (batch, num_classes)
            if len (data.y.size())==1:
                labels=data.y.unsqueeze(1)
            loss = loss_fn(softmax, labels)

            y_t += labels.tolist()
            y_p += softmax.tolist()
            x_s += list(data.sequence)
            y_l += [float(loss)]
    return y_t, y_p, y_l, x_s    

def draw_metrics(samples,target_type):
    if target_type=='gaussian':
        columns=['mape_mu','mape_sigma','loss']
        df_results = pd.DataFrame(samples, columns=columns)
    else:
        columns = ["accuracy", "extended_accuracy", "precision", " recall", "f-score", "cross-entropy"]
        df_results = pd.DataFrame(samples, columns=columns)
        df_results["accuracy"] = df_results["accuracy"] / 100
        df_results["extended_accuracy"] = df_results["extended_accuracy"] / 100

    matplotlib.rcParams["pdf.fonttype"] = 42
    matplotlib.rcParams["ps.fonttype"] = 42
    plt.rcParams.update({"font.size": 8})

    cm = 1 / 2.54  # centimeters in inches
    fig, ax = plt.subplots(1, 1, figsize=(20 * cm, 15 * cm))
    sns.set_theme(style="whitegrid")
    melted=pd.melt(df_results)
    ax = sns.boxplot(
        x="variable",
        y="value",
        data=melted,
        showfliers=False,
        notch=False,
        order=columns,
        color="tab:blue",
    )
    med_dict=melted.groupby(['variable'])['value'].median().to_dict()
    medians=[med_dict[variable] for variable in columns]
    vertical_offset = melted['value'].median() * 0.15 # offset from median for display
    ax.set_ylim([0, 1.1*max(1,max(medians))])
    for xtick in ax.get_xticks():
        ax.text(xtick,medians[xtick] - vertical_offset,round(medians[xtick],2), 
            horizontalalignment='center',size='x-small',color='w',weight='semibold',
    bbox=dict(facecolor='#445A64'))

    # plt.ticklabel_format(axis="x", style="sci", scilimits=(0,0))
    sns.despine()
    return fig
