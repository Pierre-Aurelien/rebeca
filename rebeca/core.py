"""Module to generally declutter train script."""
import argparse
from pathlib import Path

import numpy as np
import yaml


def parse_args():
    """Parse arguments."""
    parser = argparse.ArgumentParser(description="Training.")
    parser.add_argument("--neptune", type=bool, default=False, help="Log training with neptune")
    parser.add_argument(
        "--config",
        type=Path,
        help="experiment config file",
        metavar="FILE",
        required=True,
    )
    parser.add_argument("--data_name", type=str, default="nar", help="name for the dataset")
    parser.add_argument(
        "--test_model", type=str, default="my_model_best_loss.pth", help="model name to test"
    )
    parser.add_argument(
        "--architecture", type=str, default="hybrid", help="name of neural network architecture"
    )
    parser.add_argument(
        "--padding", type=bool, default=True, help="padding sequences or variable length batches"
    )

    args = parser.parse_args()
    return args


def load_cfg(yaml_filepath):
    """
    Load a YAML configuration file.

    Parameters
    ----------
    yaml_filepath : str

    Returns
    -------
    cfg : dict
    """
    # Read YAML experiment definition file
    with open(yaml_filepath, "r") as stream:
        cfg = yaml.safe_load(stream)
    # cfg = make_paths_absolute(os.path.dirname(yaml_filepath), cfg)
    return cfg
