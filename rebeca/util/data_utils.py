"""Modules containing key data transformation functions"""
import numpy as np


def encode(seq):
    """One_hot encode RNA sequence.

    Args:
        seq: mRNA 5'UTR sequence. Only AUCG letters are accepted.

    Returns:
        one hot encoding of mRNA 5'UTR sequence.
    """
    mapping = dict(zip("ACGU", range(4)))
    seq2 = [mapping[i] for i in seq]
    return np.eye(4)[seq2]
