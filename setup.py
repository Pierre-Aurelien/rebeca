"""Setup script for python packaging."""
import site
import sys

from setuptools import setup

# enable installing package for user
# https://github.com/pypa/pip/issues/7953#issuecomment-645133255
site.ENABLE_USER_SITE = "--user" in sys.argv[1:]

setup(
    name="rebeca",
    version="0.1.0",
    description="",
    author="Pierre-Aurelien Gilliot",
    zip_safe=False,
    entry_points={
        "console_scripts": [
            "hpo=rebeca.hpo:main",
            "hpo_graph=rebeca.hpo_graph:main",
            "train=rebeca.train:main",
            "train_graph=rebeca.train_graph:main",
            "test_seq=rebeca.test:main",
            "test_graph=rebeca.test_graph:main",
            "test_batch=rebeca.kosuri_batch_test:main",
            "db=rebeca.db:main",
            "tf=rebeca.transfer_learning:main",
            "tf_batch=rebeca.kosuri_batch_transfer_learning:main",
            "train_subsample=rebeca.train_subsample:main",
            "test_subsample=rebeca.test_subsample:main",
            "ft_subsample=rebeca.ft_subsample:main",
            "tf_batch_test=rebeca.kosuri_batch_test:main",
        ]
    },
)
