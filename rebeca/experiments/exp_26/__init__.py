"""EXP_26
GOAL: Investigate the baseline of heavy CONVBILSTM architectures to predict the mean and standard deviation for each construct.
METHOD: simple fepb dataset from kuo et al, using the lightweight CNN architecture. we used the KL divergence loss
"""
