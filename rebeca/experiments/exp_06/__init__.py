"""EXP_06
GOAL: Investigate the role of the dataset noise in the performance of sequence-based models
METHOD: simple fepb dataset from kuo et al, using the lightweight CNN architecture. we only used  22802 sequences for the training set
to compare with the performance on the nar dataset (equivalent size)
"""
