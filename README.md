# Rebeca

_Objective_ : Testing various neural network architectures (CNN/CNN+LSTM or GNN) to predict RBS strength.

![Protein_translation.gif](./Protein_translation.gif)

Suggestions, comments or requests can be addressed to the author of this repository:

Pierre-Aurelien Gilliot, ys18223@bristol.ac.uk

## Installation

Please clone the repository via SSH or HTTPs. e.g. in the appropriate directory, run:

```bash
  git clone https://gitlab.com/Pierre-Aurelien/rebeca.git
```

### Launching experiments

Please execute the following steps:

1. Install the conda environment (GPU powered)
   ```bash
   conda env create -f environment.yml
   ```
   If GPU are not available, run
   ```bash
   conda env create -f environment_no_gpu.yml
   ```
1. Activate the created conda environment:
   ```bash
   conda activate rebeca
   ```
1. Install the package definitions:
   ```bash
   pip install -e .
   ```

### Code Development

1. Install the conda environment (GPU powered)
   ```bash
   conda env create -f environment.yml
   ```
   If GPU are not available, run
   ```bash
   conda env create -f environment_no_gpu.yml
   ```
1. Activate the created conda environment:
   ```bash
   conda activate rebeca
   ```
1. Install pre-commit hooks
   ```bash
   pre-commit install
   ```
1. Install the package definitions:
   ```bash
   pip install -e .
   ```
