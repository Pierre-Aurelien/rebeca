"""Module for training neural network models."""
import os
import pprint
from pathlib import Path
import optuna
import neptune.new as neptune
import neptune.new.integrations.optuna as optuna_utils
import numpy as np
import pandas as pd
import torch
import torch.nn as nn
import torch.optim as optim
import torch_geometric as tg
from torch.utils.data import DataLoader
from torch_geometric.utils import degree
from torch.utils.data.sampler import SequentialSampler
from tqdm import tqdm

import rebeca.data.dataset as da
import rebeca.model.sequence_model as sm
from rebeca.core import load_cfg, parse_args
from rebeca.data.dataset import GroupedBatchSampler, UTRDataset
from rebeca.util.loss_function import kl_divergence

# from rebeca.model.sequence_model import DoubleConv, init_weights,CONVBILSTM,REC,MLP
from rebeca.util.training_utils import evaluate_graph, save_checkpoint, train_batch, val_batch

# Load args
args = parse_args()

# Load config.yml
cfg = load_cfg(args.config / "config.yml")

# Print selected configuration
pp = pprint.PrettyPrinter(indent=4)
pp.pprint(cfg)

def main():  # noqa: CCR001
    """Main script."""

    # Experiment Tracking
    if args.neptune:
        run = neptune.init_run(
            project="pag/rebeca",
            api_token=os.environ["NEPTUNE_API_TOKEN"],
            capture_stdout=False,
            capture_stderr=False,
        )
        with open(Path(args.config) / "neptune_run.txt", mode="w") as f:
            f.write(f"The url for the neptune run is {run.get_url()}")
    
    neptune_callback = optuna_utils.NeptuneCallback(run)

    study = optuna.create_study(direction="minimize")
    study.optimize(objective, n_trials=50, callbacks=[neptune_callback])

    pruned_trials = [t for t in study.trials if t.state == optuna.trial.TrialState.PRUNED]
    complete_trials = [t for t in study.trials if t.state == optuna.trial.TrialState.COMPLETE]

    print("Study statistics: ")
    print("  Number of finished trials: ", len(study.trials))
    print("  Number of pruned trials: ", len(pruned_trials))
    print("  Number of complete trials: ", len(complete_trials))

    print("Best trial:")
    trial = study.best_trial

    print("  Value: ", trial.value)

    print("  Params: ")
    for key, value in trial.params.items():
        print("    {}: {}".format(key, value))

    df = study.trials_dataframe().drop(
        ["state", "datetime_start", "datetime_complete", "duration", "number"], axis=1
    )
    print(df)
    df.to_csv(Path(args.config) / f"hpo_{Path(args.config).name}.csv", index=False)

def objective(trial):
    """Defining the objective function for optuna.

    Args:
        trial: optuna function setting hyperparameter range.
    Returns=

    """
        # Selecting hardware
    device = torch.device(cfg['GPU'] if torch.cuda.is_available() else "cpu")
    print(f"Using {device} device")

    # Reproducibility settings
    seed = 3
    np.random.seed(seed)
    torch.manual_seed(seed)
    torch.backends.cudnn.deterministic = True
    torch.backends.cudnn.benchmark = False  # switch on optimiser for cuDNN algorithms for computing convolutions, switch off for reproducibility

    # Load data
    path_raw_data=Path(cfg["param_dataset"]['path_raw_data'])
    path_root_graph = Path(cfg["param_dataset"]['path_root_graph'])
    # num_samples_train

    train_dataloader = tg.loader.DataLoader(
        getattr(da, cfg["dataset"])(root=path_root_graph / 'train',path_raw_data=path_raw_data / 'train/',
        graph_type=cfg["param_dataset"]['graph_type'],df_path=cfg["param_dataset"]['df_path'],num_samples=cfg["param_dataset"]['num_samples_train'],
        output_type=cfg["param_dataset"]['output_type'],length_sequence=cfg["param_dataset"]['length_sequence']),
        batch_size=cfg["train"]["batch_size"],
        num_workers=cfg["train"]["num_workers"],
        shuffle=True,
    )
    val_dataloader = tg.loader.DataLoader(
        getattr(da, cfg["dataset"])(root=path_root_graph / 'val',path_raw_data=path_raw_data / 'val/',
        graph_type=cfg["param_dataset"]['graph_type'],df_path=cfg["param_dataset"]['df_path'],num_samples=cfg["param_dataset"]['num_samples_val'],
        output_type=cfg["param_dataset"]['output_type'],length_sequence=cfg["param_dataset"]['length_sequence']),
        batch_size=cfg["train"]["batch_size"],
        num_workers=cfg["train"]["num_workers"],
        shuffle=True,
    )

    def get_trial_instance(type_hpo,value_hpo,key_hpo,trial):
        """given suggest_ and [64,] return trial.suggest_("key_hpo", 64,...)"""
        if type_hpo=='suggest_categorical':
            return getattr(trial,type_hpo)(key_hpo,value_hpo)
        else:
            low,high=value_hpo
        return getattr(trial,type_hpo)(key_hpo,low,high)

    dict_value=dict(cfg['value_hpo'])
    dict_type=dict(cfg['type_hpo'])

    dict_hpo={hp:get_trial_instance(type_hpo=dict_type[hp],value_hpo=vhp,key_hpo=hp,trial=trial) for (hp,vhp) in dict_value.items()}
    print('now,this',dict_hpo)

    if cfg["architecture"]["name"]=='BasePNA':
        # Compute the maximum in-degree in the training data.
        max_degree = -1
        for data in train_dataloader:
            d = degree(data.edge_index[1], num_nodes=data.num_nodes, dtype=torch.long)
            max_degree = max(max_degree, int(d.max()))

        # Compute the in-degree histogram tensor
        deg = torch.zeros(max_degree + 1, dtype=torch.long)
        for data in train_dataloader:
            d = degree(data.edge_index[1], num_nodes=data.num_nodes, dtype=torch.long)
            deg += torch.bincount(d, minlength=deg.numel())
        print('therefore')
        dict_hpo["deg"]=deg

    model = getattr(sm, cfg["architecture"]["name"])(**dict_hpo)

    # Applying weight initialisation to the architecture
    model.apply(sm.init_weights)
    print("the model is", model)
    model_parameters = filter(lambda p: p.requires_grad, model.parameters())
    nb_param = sum([np.prod(p.size()) for p in model_parameters])
    print(f"the model has {nb_param} parameters.")
    trial.set_user_attr("complexity", nb_param)


    # Specifying hardware
    if torch.cuda.is_available():
        model.to(device)  # you need to send your model weights to the GPU

    # Declare loss function and optimizer
    try: 
        loss_fn = getattr(nn, cfg["loss"]["name"])()
    except AttributeError:
        loss_fn=kl_divergence
    optimizer = getattr(optim, cfg["optimizer"]["name"])(
        model.parameters(), lr=trial.suggest_loguniform("lr", 5e-5, 5e-3)
    )
    scaler=torch.cuda.amp.GradScaler()

    min_val_loss = 100
    best_epoch = 0

    num_epochs, warmup_epochs, patience = (
        int(cfg["train"]["num_epochs"]),
        int(cfg["train"]["warmup_epochs"]),
        int(cfg["train"]["patience"]),
    )
    for epoch in range(num_epochs):  # loop over the dataset multiple times
        model.train()
        loss = []
        loss_val = []
        loop = tqdm(train_dataloader, total=int(len(train_dataloader)))
        # for data in train_dataloader:
        for _, data in enumerate(loop):
            data = data.to(device)
            loss_batch = train_batch(model, data, data.y, loss_fn, optimizer,scaler)
            loss.append(loss_batch)

        # update progress bar
        loop.set_postfix(loss=np.mean(loss))
        loop.set_description(f"Epoch [{epoch}/{num_epochs}]")

        with torch.no_grad():
            model.eval()
            for data in val_dataloader:
                data = data.to(device)
                loss_val_batch= val_batch(model, data, data.y, loss_fn)
                loss_val.append(loss_val_batch)
            val_loss_epoch = np.mean(loss_val)
            trial.report(val_loss_epoch, epoch)  # Report to optuna intermediate objective value.
        
        min_val_loss=min(min_val_loss,val_loss_epoch)
        # Handle pruning based on the intermediate value.
        if trial.should_prune():
            raise optuna.exceptions.TrialPruned()
    return min_val_loss



if __name__ == "__main__":
    main()
