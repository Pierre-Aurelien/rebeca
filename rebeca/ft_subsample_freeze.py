"""Module for training neural network architectures."""
import os
import pprint
from pathlib import Path

import neptune.new as neptune
from sklearn.model_selection import train_test_split
import numpy as np
import pandas as pd
import torch
import torch.nn as nn
import torch.optim as optim
from torch.utils.data import DataLoader
from torch.utils.data.sampler import SequentialSampler
from torch.optim.lr_scheduler import ReduceLROnPlateau
from tqdm import tqdm

import rebeca.data.dataset as da
import rebeca.model.sequence_model as sm
from rebeca.core import load_cfg, parse_args
from rebeca.data.dataset import GroupedBatchSampler, UTRDataset
from rebeca.util.loss_function import kl_divergence

# from rebeca.model.sequence_model import DoubleConv, init_weights,CONVBILSTM,REC,MLP
from rebeca.util.training_utils import evaluate, save_checkpoint, train_batch, val_batch


# Load args
args = parse_args()
# Load config.yml
cfg = load_cfg(args.config / "config.yml")

# Print selected configuration
pp = pprint.PrettyPrinter(indent=4)
pp.pprint(cfg)


def main():  # noqa: CCR001
    """Main script."""
    # Selecting hardware
    device = torch.device(cfg['GPU'] if torch.cuda.is_available() else "cpu")
    print(f"Using {device} device")

    # Reproducibility settings
    for seed in [3,7,13]:
        np.random.seed(seed)
        torch.manual_seed(seed)
        torch.backends.cudnn.deterministic = True
        torch.backends.cudnn.benchmark = False  # switch on optimiser for cuDNN algorithms for computing convolutions, switch off for reproducibility

        # Experiment Tracking
        if args.neptune:
            run = neptune.init(
                project="pag/rebeca",
                api_token=os.environ["NEPTUNE_API_TOKEN"],
                capture_stdout=False,
                capture_stderr=False,
            )
            with open(Path(args.config) / "neptune_run.txt", mode="w") as f:
                f.write(f"The url for the neptune run is {run.get_run_url()}")
        for num_samples in list(range(40,100,10))+[200,500,1e3,2e3]:#,5e3,1e4,2e4,5e4,1e5,120000]:
            print('the num samples is',num_samples)
                # Load architecture
            model = getattr(sm, cfg["architecture"]["name"])(**cfg["param_architecture"])
            model.apply(sm.init_weights)
            print("the model is", model)
            nb_param = sum(
                p.numel() for p in model.parameters() if p.requires_grad
            )  # get the number of trainable parameters.
            print(f"the model has {nb_param} parameters.")

            for model_path in Path(args.config).glob('best_model_epoch_*.pth'):
                print('Fine Tuning on', model_path)
                state_dict = torch.load(model_path, map_location=device)
                model.load_state_dict(state_dict)

                # Specifying hardware
                if torch.cuda.is_available():
                    model.to(device)  # you need to send your model weights to the GPU
                    num_workers = cfg["train"]["num_workers"]
                    batch_size = cfg["train"]["batch_size"]
                else:
                    num_workers = 0
                    batch_size = cfg["train"]["batch_size"]

                # Get indexes for training set
                df=pd.read_csv( Path(cfg['param_dataset']['path_data']) / f"{cfg['param_dataset']['journal']}_train.csv")
                bins = np.linspace(df['log_mu_f'].min(),df['log_mu_f'].quantile(0.9),4)
                df['mode_bin']=np.digitize(df['log_mu_f'], bins)
                if cfg['transfer_learning']['sample']=='stratified':
                    ID_split=train_test_split(df, test_size=int(num_samples*0.9), random_state=seed,stratify=df['mode_bin']) #18 20
                elif cfg['transfer_learning']['sample']=='random':
                    ID_split=train_test_split(df, test_size=int(num_samples*0.9), random_state=seed)
                # print(ID_split[1].index.to_list(),len(ID_split[1]))
                train_data=torch.utils.data.Subset(getattr(da, cfg["dataset"])(split="train", **cfg["param_dataset"]), ID_split[1].index.to_list())
                
                # Get indexes for test set
                df=pd.read_csv( Path(cfg['param_dataset']['path_data']) / f"{cfg['param_dataset']['journal']}_val.csv")
                bins = np.linspace(df['log_mu_f'].min(),df['log_mu_f'].quantile(0.9),4)
                df['mode_bin']=np.digitize(df['log_mu_f'], bins)
                if cfg['transfer_learning']['sample']=='stratified':
                    ID_split=train_test_split(df, test_size=min(int(num_samples*0.1),len(df)), random_state=seed,stratify=df['mode_bin']) #18 20
                elif cfg['transfer_learning']['sample']=='random':
                    ID_split=train_test_split(df, test_size=min(int(num_samples*0.1),len(df)), random_state=seed)
                # print('this time',ID_split[1].index.to_list(),len(ID_split[1]))
                val_data=torch.utils.data.Subset(getattr(da, cfg["dataset"])(split="val", **cfg["param_dataset"]), ID_split[1].index.to_list())


                # Load data
                train_dataloader = DataLoader(
                    train_data,
                    batch_size=batch_size,
                    shuffle=True,
                    num_workers=num_workers,
                    pin_memory=True,
                )
                val_dataloader = DataLoader(
                    val_data,
                    batch_size=batch_size,
                    shuffle=True,
                    num_workers=num_workers,
                    pin_memory=True,
                )
                for (n,p) in model.named_parameters():
                    if 'mlp' in n:
                        print('indeed')
                        pass
                    else:
                        p.requires_grad=False


                try: 
                    loss_fn = getattr(nn, cfg["loss"]["name"])()
                except AttributeError:
                    loss_fn=kl_divergence
                optimizer = getattr(optim, cfg["optimizer"]["name"])(
                    model.parameters(), lr=float(cfg["optimizer"]["lr"])/10
                )

                scheduler = ReduceLROnPlateau(optimizer, mode='min', factor=0.5, patience=5,
                                        min_lr=1e-5)

                scaler=torch.cuda.amp.GradScaler()

                best_loss = 100
                best_epoch = 0
                num_epochs, warmup_epochs, patience = (
                    500,
                    int(cfg["train"]["warmup_epochs"]),
                    int(cfg["train"]["patience"]),
                )
                for epoch in range(num_epochs):  # loop over the dataset multiple times
                    model.train()
                    loss = []
                    acc = []
                    loss_val = []
                    acc_val = []
                    loop = tqdm(train_dataloader, total=int(len(train_dataloader)))
                    for _, data in enumerate(loop):
                        inputs, labels, _ = data  # get the inputs; data is a list of [inputs, labels]
                        inputs, labels = inputs.to(device), labels.to(device)  # Send input tensors to the GPU
                        loss_batch, acc_batch = train_batch(model, inputs, labels, loss_fn, optimizer,scaler)
                        loss.append(loss_batch)
                        acc.append(acc_batch)
                        if args.neptune:
                            # log on neptune
                            run["train/batch/loss"].log(loss_batch)
                            run["train/batch/accuracy"].log(acc_batch)
                    if args.neptune:
                        # log on neptune
                        run["train/batch/epoch_loss"].log(np.mean(loss))
                        run["train/batch/epoch_accuracy"].log(np.mean(acc))
                    # update progress bar
                    loop.set_postfix(loss=np.mean(loss), accuracy=np.mean(acc))
                    loop.set_description(f"Epoch [{epoch}/{num_epochs}]")

                    with torch.no_grad():
                        model.eval()
                        for data in val_dataloader:
                            inputs, labels, _ = data
                            inputs, labels = inputs.to(device), labels.to(
                                device
                            )  # Send input tensors to the GPU
                            _, loss_val_batch, acc_val_batch, _ = val_batch(
                                model, inputs, labels, loss_fn
                            )
                            loss_val.append(loss_val_batch)
                            acc_val.append(acc_val_batch)
                            if args.neptune:
                                run["val/batch/accuracy"].log(acc_val_batch)
                                run["val/batch/loss"].log(loss_val_batch)
                        val_loss_epoch = np.mean(loss_val)
                        scheduler.step(val_loss_epoch)
                        if args.neptune:
                            run["train/distribution"].log(
                                evaluate(val_dataloader, model, epoch, "validation", device)
                            )
                            run["val/batch/epoch_accuracy"].log(np.mean(acc_val_batch))
                            run["val/batch/epoch_loss"].log(np.mean(loss_val))
                        if val_loss_epoch < best_loss and epoch > warmup_epochs:
                            best_loss = val_loss_epoch
                            best_epoch = epoch
                            # keep snapshot
                            best_model = model.state_dict()
                    if epoch - best_epoch > patience:
                        print("training finished")
                        if args.neptune:
                            # Log PyTorch model weights
                            run["model_checkpoints/my_model"].upload(
                                f"model_checkpoints/best_model_epoch_{epoch}.pth"
                            )
                        break
                # At this point save a snapshot of the best model
                torch.save(best_model, Path(args.config) / f"ft_samples_{int(num_samples)}_seed_{seed}.pth")
                if args.neptune:
                    run.stop()


if __name__ == "__main__":
    main()
