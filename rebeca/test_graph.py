"""Module for testing the performance of a neural network architectures."""

import pprint
from collections import OrderedDict
from pathlib import Path
from itertools import chain

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
import torch
import torch.nn as nn
from sklearn.metrics import confusion_matrix
from torch.utils.data import DataLoader
import torch_geometric as tg
import rebeca.data.dataset as da
import rebeca.model.sequence_model as sm
from rebeca.core import load_cfg, parse_args
from rebeca.util.testing_utils import bootstrap_estimate_and_ci, draw_metrics, get_output_graph

from tqdm import tqdm

matplotlib.rcParams["pdf.fonttype"] = 42
matplotlib.rcParams["ps.fonttype"] = 42
plt.rcParams.update({"font.size": 8})


def evaluate(data_loader, model, split, loss_fn,target_type):

    y_t, y_p, y_l, x_s = get_output_graph(data_loader, model, loss_fn)

    print(y_t,y_p,'hey')

    df_aux = pd.DataFrame(
        [[x_s[i], y_p[i], y_t[i]] for i in range(len(x_s))], columns=["seq", "prediction", "truth"]
    )
    df_aux["rbs"] = df_aux.apply(lambda row: row["seq"].replace("Z", ""), axis=1)
    y_pred = (
        df_aux.groupby(["rbs"])[["prediction", "truth"]]
        .agg(lambda x: pd.Series.mode(x)[0])["prediction"]
        .to_list()
    )
    y_true = (
        df_aux.groupby(["rbs"])[["prediction", "truth"]]
        .agg(lambda x: pd.Series.mode(x)[0])["truth"]
        .to_list()
    )

    nb_samples = 1
    macro_metrics, samples = bootstrap_estimate_and_ci(target_type='gaussian',y_true=y_true, y_pred=y_pred, loss_value=y_l, bs_samples=nb_samples)
    fig_bs = draw_metrics(samples,target_type='gaussian')
    fig='nothing'


    # Build confusion matrix
    # fig, ax = plt.subplots(1, 1,figsize=(25, 20)) configuration for slides

   



    return fig, macro_metrics, samples, fig_bs


def main():  # noqa: CCR001
    """Main script."""
    # Selecting hardware, Load on CPU
    device = torch.device("cuda:0")
    print(f"Using {device} device")

    # Reproducibility settings
    seed = 3
    np.random.seed(seed)
    torch.manual_seed(seed)
    torch.backends.cudnn.deterministic = True
    torch.backends.cudnn.benchmark = False  # switch on optimiser for cuDNN algorithms for computing convolutions, switch off for reproducibility

    # Load args
    args = parse_args()

    # Load config.yml
    cfg = load_cfg(args.config / "config.yml")

    # Print selected configuration
    pp = pprint.PrettyPrinter(indent=4)
    pp.pprint(cfg)

    # Loadmodel
    model = getattr(sm, cfg["architecture"]["name"])(**cfg["param_architecture"])
    model.apply(sm.init_weights)
    print("the model is", model)

    # Load data
    path_raw_data=Path(cfg["param_dataset"]['path_raw_data'])
    path_root_graph = Path(cfg["param_dataset"]['path_root_graph'])

    print(path_raw_data,path_root_graph)
    test_dataloader = tg.loader.DataLoader(
        getattr(da, cfg["dataset"])(root=path_root_graph / 'test',path_raw_data=path_raw_data / 'test',
        graph_type=cfg["param_dataset"]['graph_type'],df_path=cfg["param_dataset"]['df_path'],num_samples=cfg["param_dataset"]['num_samples_test'],
        output_type=cfg["param_dataset"]['output_type'],length_sequence=cfg["param_dataset"]['length_sequence']),
        batch_size=32,
        num_workers=18,
        shuffle=True,
    )


    loss_fn = getattr(nn, cfg["loss"]["name"])()

    for model_path in Path(args.config).glob("*.pth"):
        # original saved file with DataParallel
        epoch = model_path.stem.split("l_")[1]
        name_model = model_path.stem

        state_dict=torch.load(model_path, map_location=device)

        model.load_state_dict(state_dict)
        nb_param = sum(
            p.numel() for p in model.parameters() if p.requires_grad
        )  # get the number of trainable parameters.
        print(f"the model has {nb_param} parameters.")
        # fig,macro_metrics, samples, fig_bs = evaluate(test_dataloader, model, "test", loss_fn,target_type='gaussian')

        
        sequence=[]
        output=[]
        mu_label=[]
        g_t=[]


        with torch.no_grad():
            model.to(device) 
            model.eval()
            loop = tqdm(test_dataloader, total=int(len(test_dataloader)))
            for _, data in enumerate(loop):
                data = data.to(device)
                softmax = model(data)  # (batch, num_classes)
                if len (data.y.size())==1:
                    labels=data.y.unsqueeze(1)
                loss = loss_fn(softmax, labels)

                mu_label += labels.tolist()
                output += softmax.tolist()
                sequence += list(data.sequence)

        

        # df = pd.DataFrame(data={"seq": sequence,"output": list(chain.from_iterable(output)),"label": list(chain.from_iterable(g_t))})
        df = pd.DataFrame(data={"seq": sequence,"output": list(chain.from_iterable(output)),"label": list(chain.from_iterable(mu_label))})
        # "sd_output": list(sd_output),
        # "mu_label": list(mu_label),
        # "sd_label": list(sd_label)})
        df.to_csv(Path(args.config) /f'complete_output_{name_model}.csv', sep=',',index=False)

        from scipy import stats

        
        df_aux=df
        # df_aux=df_aux[df_aux['label']>3]
        # df_aux['label']/=df_aux['label'].max()
        # get coeffs of linear fit
        slope, intercept, r_value, p_value, std_err = stats.linregress(df_aux['label'],df_aux['output'])
        mae=np.mean(np.abs(df_aux['label']-df_aux['output']))
        from scipy import stats
        print('the sem is:',stats.sem(np.abs(df_aux['label']-df_aux['output'])))


        cm = 1/2.54  # centimeters in inches
        fig, ax = plt.subplots(1, 1,figsize=(6*cm, 6*cm))
        # # use line_kws to set line label for legend
        ax=sns.scatterplot(data=df_aux, x="label", y="output", color = 'silver',s=0.8)
        # ax = sns.regplot(x="label", y="output", data=df_aux, scatter = False,ci=95,color='black',label="y={0:.4f}x+{1:.1f}, R={2:.2f}. MAE={3:.4f},sd={4:.4f}".format(slope,intercept,r_value,mae,std_err))
        ax = sns.regplot(x="label", y="output", data=df_aux, scatter = False,ci=95,color='black',label="y={0:.2f}x+{1:.1f}".format(slope,intercept,r_value,mae,std_err))
        # ax = sns.regplot(x="label", y="output", data=df_aux,scatter_kws={"s": 0.01,"color":"siver"},line_kws={'label':"y={0:.5f}x+{1:.1f}, R={2:.5f}".format(slope,intercept,r_value),"color":"black"})

        # plot legend
        ax.set_xlabel('Ground Truth',fontsize=8)
        ax.set_ylabel('Prediction',fontsize=8)
        ax.legend(frameon=False)
        sns.despine()
        plt.ylim([0,1])
        sns.set_style("white")
        sns.set_style("ticks")
        # plt.xlim([0,1])
        plt.savefig(Path(args.config) /f'{Path(args.config).stem}.pdf', transparent=True, bbox_inches='tight')

        # with torch.no_grad():
        #     model.eval()
        #     for data in test_dataloader:
        #         data = data.to(device)
        #         inputs,labels=data, data.y
        #         softmax = model(inputs)  # (batch, num_classes)
        #         if len (labels.size())==1:
        #             print(labels.size(),'before')
        #             labels=labels.unsqueeze(1)
        #             print(labels.size(),'after')
        #         loss = loss_fn(softmax, labels)
        #         print(loss,'hey')


        # print(macro_metrics)
        # print("metrics averaged are", macro_metrics[0, :])
        # plt.tight_layout()
        # fig.savefig(
        #     Path(args.config) / f"confusion_matrix_{epoch}.pdf",
        #     transparent=True,
        #     bbox_inches="tight",
        # )
        # sns.set_theme(style="whitegrid")
        # fig_bs.savefig(
        #     Path(args.config) / "performance_nn.pdf", transparent=True, bbox_inches="tight"
        # )
        # with open(Path(args.config) / f"metrics_{epoch}.txt", mode="w") as f:
        #     f.write(
        #         f"The macro metrics (accuracy, extended_accuracy,precision, recall, F-measure) and "
        #         f"their estimates, lower/upper bounds and std_deviation are given by the following 4*5 matrix:\n {macro_metrics}"
        #     )
        # np.save(Path(args.config) / "bootstrap_metrics.npy", samples)


if __name__ == "__main__":
    main()
