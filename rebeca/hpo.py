"""Module for training sequence-based neural network architectures."""
import os
import pprint
from pathlib import Path
import neptune.new as neptune
import neptune.new.integrations.optuna as optuna_utils
import numpy as np
import optuna
import torch
import torch.nn as nn
import torch.optim as optim
from torch.utils.data import DataLoader
from tqdm import tqdm
# from rebeca.data.dataset import UTRDataset
import rebeca.data.dataset as da
import rebeca.model.sequence_model as sm
from rebeca.core import load_cfg, parse_args
from rebeca.model.sequence_model import CONVBILSTM, MLP, DoubleConv, init_weights
from rebeca.util.loss_function import kl_divergence
from rebeca.util.training_utils import evaluate, save_checkpoint, train_batch, val_batch

# Load args
args = parse_args()

# Load config.yml
cfg = load_cfg(args.config / "config.yml")

# Print selected configuration
pp = pprint.PrettyPrinter(indent=4)
pp.pprint(cfg)

# def custom_dataloader(journal, split, batch_size, num_workers):
#     """Return Dataloader."""
#     return DataLoader(
#         UTRDataset(journal=f"{journal}", split=f"{split}", utr_max_length=30),
#         batch_size=batch_size,
#         shuffle=True,
#         num_workers=num_workers,
#         pin_memory=True,
#     )


# def train_batch(model, inputs, labels, loss_fn, optimizer):
#     """Defines the training step after one forward pass of one batch of data."""
#     for param in model.parameters():  # zero the parameter gradients
#         param.grad = None
#     softmax = model(inputs)  # (batch, num_classes)
#     loss = loss_fn(softmax, labels)
#     loss.backward()  # compute updates for each parameter
#     optimizer.step()  # make the updates for each parameter

#     preds = softmax.argmax(dim=1)  # (batch,)
#     num_correct_train = int((preds == labels).sum().cpu().numpy())
#     acc_train = num_correct_train / len(preds)
#     return float(loss.detach().cpu().numpy()), acc_train


# def val_batch(model, inputs, labels, loss_fn):
#     """Validating step using a batch of data."""
#     softmax = model(inputs)  # (batch, num_classes)
#     loss = loss_fn(softmax, labels)
#     preds = softmax.argmax(dim=1)  # (batch,)
#     num_correct_val = int((preds == labels).sum().cpu().numpy())
#     num_sample_val = len(preds)
#     acc_val = num_correct_val / num_sample_val
#     return preds, float(loss.detach().cpu().numpy()), acc_val, softmax


# def save_checkpoint(architecture, optimizer, best_ckpt_path, epoch):
#     """Save the model checkpoint."""
#     state = {
#         "epoch": epoch,
#         "state_dict": architecture.state_dict(),
#         "optimizer": optimizer.state_dict(),
#     }
#     torch.save(state, best_ckpt_path)


def objective(trial):
    """Defining the objective function for optuna.

    Args:
        trial: optuna function setting hyperparameter range.
    Returns:

    """
    
    #Reproducibility settings
    seed = 3
    np.random.seed(seed)
    torch.manual_seed(seed)
    torch.backends.cudnn.deterministic = True
    torch.backends.cudnn.benchmark = False  # switch on optimiser for cuDNN

    device = torch.device(cfg['GPU'] if torch.cuda.is_available() else "cpu")
    print(f"Using {device} device")

    def get_trial_instance(type_hpo,value_hpo,key_hpo,trial):
        """given suggest_ and [64,] return trial.suggest_("key_hpo", 64,...)"""
        if type_hpo=='suggest_categorical':
            return getattr(trial,type_hpo)(key_hpo,value_hpo)
        else:
            low,high=value_hpo
        return getattr(trial,type_hpo)(key_hpo,low,high)

    dict_value=dict(cfg['value_hpo'])
    dict_type=dict(cfg['type_hpo'])

    dict_hpo={hp:get_trial_instance(type_hpo=dict_type[hp],value_hpo=vhp,key_hpo=hp,trial=trial) for (hp,vhp) in dict_value.items()}
    print('now,this',dict_hpo)
    model = getattr(sm, cfg["architecture"]["name"])(**dict_hpo)
    
    # Applying weight initialisation to the architecture
    model.apply(sm.init_weights)
    print("the model is", model)
    nb_param = sum(
        p.numel() for p in model.parameters() if p.requires_grad
    )  # get the number of trainable parameters.
    trial.set_user_attr("complexity", nb_param)
    print(f"the model has {nb_param} parameters.")


    # Create NN model
    # architecture=MLP(classes=8,
    #     hidden_size_mlp=trial.suggest_categorical("hidden_size_mlp", [64,128,256,512,1024]),
    #     layers_mlp=trial.suggest_int("layers_mlp", 1, 5),
    #     activation=trial.suggest_categorical('activation', ["ReLU","ELU","LeakyReLU"]),
    #     activation_out= "",
    #     dropout_rate=trial.suggest_categorical("dropout_rate", [0,0.05,0.1,0.2,0.3,0.5]),)

    # architecture = DoubleConv(
    #     classes=8,
    #     channels_cnn=trial.suggest_categorical("conv_channels", [64, 128, 256, 512]),
    #     layers_cnn=trial.suggest_int("conv_layers", 0, 2),
    #     kernel_cnn=trial.suggest_categorical("conv_kernel", [4, 6, 8, 10]),
    #     kernel_pool=trial.suggest_int("kernel_pool", 1, 2),
    #     hidden_size_mlp=trial.suggest_categorical("MLP_units_l", [64, 128, 256, 512, 1024]),
    #     layers_mlp=trial.suggest_int("MLP_layers", 1, 3),
    #     activation=trial.suggest_categorical("activation", ["ReLU", "ELU", "LeakyReLU"]),
    #     activation_out="",
    #     dropout_rate=trial.suggest_categorical("dropout_l", [0, 0.05, 0.1, 0.2, 0.3, 0.5]),
    # ).to(device)

    # architecture = CONVBILSTM(classes=8,
    # channels_cnn=trial.suggest_categorical('conv_channels', [64,128,256,512]),
    # layers_cnn=trial.suggest_int("conv_layers", 0, 2),
    # kernel_cnn=trial.suggest_categorical('conv_kernel', [4,6,8,10]),
    # kernel_pool=trial.suggest_int("kernel_pool", 1, 2),
    # hidden_size_lstm=trial.suggest_categorical('hidden_size_lstm', [5,10,25,50,100,200,500]),
    # layers_lstm=1,
    # bilstm=trial.suggest_categorical("BiLSTM", [True,False]),
    # hidden_size_mlp=trial.suggest_categorical("MLP_units", [64,128,256,512,1024]),
    # layers_mlp=trial.suggest_int("MLP_layers", 1, 3),
    # activation =trial.suggest_categorical('activation', ["ReLU","ELU","LeakyReLU"]),
    # activation_out = "",
    # dropout_rate = trial.suggest_categorical("dropout_l", [0,0.05,0.1,0.2,0.3,0.5])).to(device)

    if torch.cuda.is_available():
        model.to(device)  # you need to send your model weights to the GPU
        num_workers = 18
        if torch.cuda.device_count() > 1:
        #     model=nn.DataParallel(model)
            model.to(device)  # you need to send your model weights to the GPU
            num_workers = 18
        # batch_size = cfg["train"]["batch_size"] * torch.cuda.device_count()
    else:
        num_workers = 4
        batch_size = cfg["train"]["batch_size"]


    # import time
    # pin_memory = True
    # print('pin_memory is', pin_memory)

    # for num_workers in range(0, 20, 1): 
    #     train_loader = DataLoader(
    #     getattr(da, cfg["dataset"])(split="train", **cfg["param_dataset"]),
    #     batch_size=64,
    #     shuffle=True,
    #     num_workers=num_workers,
    #     pin_memory=True,
    # )
    #     start = time.time()
    #     for epoch in range(1, 5):
    #         for i, data in enumerate(train_loader):
    #             pass
    #     end = time.time()
    #     print("Finish with:{} second, num_workers={}".format(end - start, num_workers))

    #Load data
    train_dataloader = DataLoader(
        getattr(da, cfg["dataset"])(split="train", **cfg["param_dataset"]),
        batch_size=trial.suggest_categorical("batch_size",[32,64]),
        shuffle=True,
        num_workers=num_workers,
        pin_memory=True,
    )
    val_dataloader = DataLoader(
        getattr(da, cfg["dataset"])(split="val", **cfg["param_dataset"]),
        batch_size=1024,
        shuffle=True,
        num_workers=num_workers,
        pin_memory=True,
    )

    # Declare loss function and optimizer
    try: 
        loss_fn = getattr(nn, cfg["loss"]["name"])()
    except AttributeError:
        loss_fn=kl_divergence
    optimizer = getattr(optim, cfg["optimizer"]["name"])(
        model.parameters(), lr=trial.suggest_loguniform("lr", 1e-4, 1e-2)
    )
    scaler=torch.cuda.amp.GradScaler()
    # # Load data
    # train_dataloader = custom_dataloader("nar", "train", 128, num_workers)
    # val_dataloader = custom_dataloader("nar", "val", 128, num_workers)
    min_val_loss = 100
    best_epoch = 0
    num_epochs, warmup_epochs, patience = (
        int(cfg["train"]["num_epochs"]),
        int(cfg["train"]["warmup_epochs"]),
        int(cfg["train"]["patience"]),
    )

    for epoch in range(num_epochs):  # loop over the dataset multiple times
        model.train()
        loss = []
        loss_val=[]
        loop = tqdm(train_dataloader, total=int(len(train_dataloader)))
        for _, data in enumerate(loop):
            # get the inputs; data is a list of [inputs, labels]
            inputs, labels, _ = data
            inputs, labels = inputs.to(device), labels.to(device)  # Send input tensors to the GPU
            loss_batch = train_batch(model, inputs, labels, loss_fn, optimizer,scaler)
            loss.append(loss_batch)
        # update progress bar
        loop.set_postfix(loss=np.mean(loss))
        loop.set_description(f"Epoch [{epoch}/{num_epochs}]")

        with torch.no_grad():
            model.eval()
            for data in val_dataloader:
                inputs, labels, _ = data
                inputs, labels = inputs.to(device), labels.to(
                    device
                )  # Send input tensors to the GPU
                loss_val_batch=val_batch(
                    model, inputs, labels, loss_fn
                )  # noqa: W0612
                loss_val.append(loss_val_batch)
            val_loss_epoch = np.mean(loss_val)

            trial.report(val_loss_epoch, epoch)  # Report to optuna intermediate objective value.
            min_val_loss=min(min_val_loss,val_loss_epoch)
        # Handle pruning based on the intermediate value.
        if trial.should_prune():
            raise optuna.exceptions.TrialPruned()

    return min_val_loss


def main():  # noqa: CCR001
    """Main script."""

    # Experiment Tracking
    if args.neptune:
        run = neptune.init_run(
            project="pag/rebeca",
            api_token=os.environ["NEPTUNE_API_TOKEN"],
            capture_stdout=False,
            capture_stderr=False,
        )
        with open(Path(args.config) / "neptune_run.txt", mode="w") as f:
            f.write(f"The url for the neptune run is {run.get_url()}")
    neptune_callback = optuna_utils.NeptuneCallback(run)

    study = optuna.create_study(direction="minimize")
    study.optimize(objective, n_trials=200, callbacks=[neptune_callback])

    pruned_trials = [t for t in study.trials if t.state == optuna.trial.TrialState.PRUNED]
    complete_trials = [t for t in study.trials if t.state == optuna.trial.TrialState.COMPLETE]

    print("Study statistics: ")
    print("  Number of finished trials: ", len(study.trials))
    print("  Number of pruned trials: ", len(pruned_trials))
    print("  Number of complete trials: ", len(complete_trials))

    print("Best trial:")
    trial = study.best_trial

    print("  Value: ", trial.value)

    print("  Params: ")
    for key, value in trial.params.items():
        print("    {}: {}".format(key, value))

    df = study.trials_dataframe().drop(
        ["state", "datetime_start", "datetime_complete", "duration", "number"], axis=1
    )
    print(df)
    df.to_csv(Path(args.config) / f"hpo_{Path(args.config).name}.csv", index=False)


if __name__ == "__main__":
    main()
