"""Module for training neural network architectures."""
import neptune.new as neptune
import numpy as np
import pandas as pd
import torch
import torch.nn as nn
import torch.optim as optim
import os
import pprint
from torch.optim import lr_scheduler
from torch.optim.lr_scheduler import ReduceLROnPlateau
from torch.utils.data import DataLoader
from tqdm import tqdm
from pathlib import Path
import rebeca.data.dataset as da
import rebeca.model.sequence_model as sm
from rebeca.core import load_cfg, parse_args
from rebeca.data.dataset import UTRDataset
from rebeca.model.sequence_model import CONVBILSTM, MLP, DoubleConv, init_weights
from rebeca.util.training_utils import evaluate, save_checkpoint, train_batch, val_batch



def main():  # noqa: CCR001
    """Main script."""
    # Reproducibility settings
    seed = 3
    # random.seed(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)
    torch.backends.cudnn.deterministic = True
    torch.backends.cudnn.benchmark = False  # switch on optimiser for cuDNN algorithms for computing convolutions, switch off for reproducibility

    # Load args
    args = parse_args()
    if args.neptune:
        run = neptune.init(
            project="pag/rebeca",
            api_token=os.environ["NEPTUNE_API_TOKEN"],
            capture_stdout=False,
            capture_stderr=False,
        )
    
    # Load config.yml
    cfg = load_cfg(args.config / "config.yml")

    # Print selected configuration
    pp = pprint.PrettyPrinter(indent=4)
    pp.pprint(cfg)

    # Selecting hardware
    device = torch.device(cfg['GPU'] if torch.cuda.is_available() else "cpu")
    print(f"Using {device} device")

    model = getattr(sm, cfg["architecture"]["name"])(**cfg["param_architecture"])
    # architecture = CONVBILSTM(**best_param).to(device)
    
    df_total=pd.read_csv(Path(cfg['param_dataset']['path_data']) / f"all_ft_train.csv")
    list_prom=df_total['promoter'].unique().tolist()
    for promoter in list_prom:
        cfg['param_dataset']['promoter']=promoter
        path_model=Path(args.config)/'starting_model.pth'
        # path_model=list(Path(args.config).glob("*.pth"))[0]
        model.load_state_dict(
            torch.load(path_model, map_location=device)
        )  # Choose whatever GPU device number you want

        # for param in model.parameters():
        #     param.requires_grad = False

        # # Parameters of newly constructed modules have requires_grad=True by default
        # model.fc = nn.Linear(1, 1)

        # # Observe that only parameters of final layer are being optimized as
        # # opposed to before.
        # optimizer = optim.SGD(model.fc.parameters(), lr=0.0001, momentum=0.9)


        optimizer = getattr(optim, cfg["optimizer"]["name"])(
        model.parameters(), lr=float(cfg["optimizer"]["lr"])/10
    )
        print('the learning rate is now',float(cfg["optimizer"]["lr"])/10)
        # Declare loss function and optimizer
        try: 
            loss_fn = getattr(nn, cfg["loss"]["name"])()
        except AttributeError:
            loss_fn=kl_divergence
        
        scheduler = ReduceLROnPlateau(optimizer, mode='min', factor=0.5, patience=3,threshold=0.01,
                                min_lr=1e-5)

        scaler=torch.cuda.amp.GradScaler()
        # Specifying hardware
        if torch.cuda.is_available():
            model.to(device)  # you need to send your model weights to the GPU
            num_workers = 0
            batch_size = 16 * torch.cuda.device_count()
        else:
            num_workers = 0
            batch_size = 16

        # Load data
        train_dataloader = DataLoader(
            getattr(da, cfg["dataset"])(split="train", **cfg["param_dataset"]),
            batch_size=batch_size,
            shuffle=True,
            num_workers=num_workers,
            pin_memory=True,
        )
        val_dataloader = DataLoader(
            getattr(da, cfg["dataset"])(split="val", **cfg["param_dataset"]),
            batch_size=batch_size,
            shuffle=True,
            num_workers=num_workers,
            pin_memory=True,
        )
        nb_param = sum(
            p.numel() for p in model.parameters() if p.requires_grad
        )  # get the number of trainable parameters.
        print(f"the model has {nb_param} trainable parameters.")

        # Parameters

        patience=int(cfg["train"]["patience"])
        best_loss = 100
        best_epoch = 0
        num_epochs, warmup_epochs = (
            600,
            int(cfg["train"]["warmup_epochs"])
        )
        for epoch in range(num_epochs):  # loop over the dataset multiple times
            model.train()
            loss = []
            acc = []
            loss_val = []
            acc_val = []
            loop = tqdm(train_dataloader, total=int(len(train_dataloader)))
            for _, data in enumerate(loop):
                inputs, labels, _ = data  # get the inputs; data is a list of [inputs, labels]
                inputs, labels = inputs.to(device), labels.to(device)  # Send input tensors to the GPU
                loss_batch, acc_batch = train_batch(model, inputs, labels, loss_fn, optimizer,scaler)
                loss.append(loss_batch)
                acc.append(acc_batch)
                if args.neptune:
                    # log on neptune
                    run["train/batch/loss"].log(loss_batch)
                    run["train/batch/accuracy"].log(acc_batch)
            if args.neptune:
                # log on neptune
                run["train/batch/epoch_loss"].log(np.mean(loss))
                run["train/batch/epoch_accuracy"].log(np.mean(acc))
            # update progress bar
            loop.set_postfix(loss=np.mean(loss), accuracy=np.mean(acc))
            loop.set_description(f"Epoch [{epoch}/{num_epochs}]")

            with torch.no_grad():
                model.eval()
                for data in val_dataloader:
                    inputs, labels, _ = data
                    inputs, labels = inputs.to(device), labels.to(
                        device
                    )  # Send input tensors to the GPU
                    _, loss_val_batch, acc_val_batch, _ = val_batch(
                        model, inputs, labels, loss_fn
                    )
                    loss_val.append(loss_val_batch)
                    acc_val.append(acc_val_batch)
                    if args.neptune:
                        run["val/batch/accuracy"].log(acc_val_batch)
                        run["val/batch/loss"].log(loss_val_batch)
                val_loss_epoch = np.mean(loss_val)
                scheduler.step(val_loss_epoch)
                if args.neptune:
                    run["train/distribution"].log(
                        evaluate(val_dataloader, model, epoch, "validation", device)
                    )
                    run["val/batch/epoch_accuracy"].log(np.mean(acc_val_batch))
                    run["val/batch/epoch_loss"].log(np.mean(loss_val))
                if val_loss_epoch < best_loss and epoch > warmup_epochs:
                    best_loss = val_loss_epoch
                    best_epoch = epoch
                    # keep snapshot
                    best_model = model.state_dict()
            if epoch - best_epoch > patience:
                print("training finished")
                if args.neptune:
                    # Log PyTorch model weights
                    run["model_checkpoints/my_model"].upload(
                        f"model_checkpoints/best_model_epoch_{epoch}.pth"
                    )
                break
        # At this point save a snapshot of the best model
        torch.save(best_model, Path(args.config) / f"best_ft_model_promoter_{cfg['param_dataset']['promoter']}.pth")
        if args.neptune:
            run.stop()


if __name__ == "__main__":
    main()
